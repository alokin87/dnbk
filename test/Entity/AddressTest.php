<?php

namespace Dnbk\Tests\Entity;

use PHPUnit_Framework_TestCase as TestCase;
use Dnbk\Domain\Entity\Address\Address;
use Dnbk\Domain\Entity\Address\Street;
use Dnbk\Domain\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class AddressTest extends TestCase
{
    public function testArrayCreation()
    {
        $address = Address::createFromArray([
            'street' => ['name' => 'Test'],
            'number' => 10
        ]);

        $this->assertInstanceOf(Street::class, $address->getStreet());
        $this->assertEquals(10, $address->getStreetNumber());
    }

    public function testArrayCreationFailsIfStreetIsMissing()
    {
        $this->setExpectedException(Exception\InvalidAddressException::class,
            'street name must be supplied'
        );

        Address::createFromArray(['number' => 1]);
    }

    public function testArrayCreationFailsIfNumberIsMissing()
    {
        $this->setExpectedException(Exception\InvalidAddressException::class,
            'number must be supplied'
        );

        Address::createFromArray(['street' => ['name' => 'Test']]);
    }

    public function testValidation()
    {
        $this->setExpectedException(Exception\InvalidAddressException::class,
            'number must be supplied'
        );

        Address::create(Street::create('Test'), '    ');
    }

    public function testComparison()
    {
        $this->assertTrue(Address::create(Street::create('Test'), 1)->equals(Address::create(Street::create('Test'), 1)));

        $this->assertFalse(Address::create(Street::create('Test'), 10)->equals(Address::create(Street::create('Test'), 20)));
    }

    public function testRangeChecking()
    {
        $address = Address::create(Street::create('Test'), '7a');

        $this->assertTrue($address->isInRange(1, 7));
        $this->assertTrue($address->isInRange(1, 9));
        $this->assertFalse($address->isInRange(2, 10));
    }
}
