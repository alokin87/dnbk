<?php

namespace Dnbk\Domain\Repository;

use Dnbk\Domain\Entity\Subscription;
use Dnbk\Container\ContainerAwareInterface;
use Dnbk\Container\ContainerAwareTrait;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class SubscriptionRepository implements SubscriptionRepositoryInterface, ContainerAwareInterface
{
    USE ContainerAwareTrait;

    /**
     * @var Subscription
     */
    private $model;

    /**
     * @var ContactRepositoryInterface
     */
    private $contactRepository;

    public function __construct(Subscription $subscriptionModel = null)
    {
        $this->model = $subscriptionModel ?: new Subscription();
    }

    public function setContactRepository(ContactRepositoryInterface $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    /**
     * @return ContactRepositoryInterface
     */
    private function getContactRepository()
    {
        if (null === $this->contactRepository) {
            $this->setContactRepository($this->getContainer()->get(ContactRepositoryInterface::class));
        }

        return $this->contactRepository;
    }

    public function findAll()
    {
        return $this->model->with('contact')->get();
    }

    public function findActive()
    {
        return $this->model->with('contact')->where(['status' => Subscription::STATUS_ACTIVE])->get();
    }

    public function findByUid($uid)
    {
        return $this->model->where(['uid' => $uid])->first();
    }

    public function find(Subscription $subscription)
    {
        $existingSubscription = $this->model->where([
            'address_name' => $subscription->getAttribute('address_name'),
            'address_municipality' => $subscription->getAttribute('address_municipality'),
            'address_number' => $subscription->getAttribute('address_number'),
        ])->first();

        if (null !== $existingSubscription) {
            if (null !== $this->getContactRepository()->findOne($subscription->getContact())) {
                return $existingSubscription;
            }
        }

        return null;
    }

    public function save(Subscription $subscription)
    {
        if (!$subscription->exists) {
            if (($existingContact = $this->getContactRepository()->findOne($subscription->getContact()))) {
                $subscription->setContact($existingContact);
            }

            $subscription->isDirty();

            $subscription->getContact()->save();

            $subscription->setAttribute('contact_id', $subscription->getContact()->getId());
        }

        $subscription->save();
    }

    public function remove(Subscription $subscription)
    {
        $subscription->delete();
    }
}
