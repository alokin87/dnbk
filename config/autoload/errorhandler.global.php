<?php

return [
    'error_handler' => [
        'display_exceptions' => false,
        'handlers' => [
            'html' => [
                'templates' => [
                    '404'   => 'error::404',
                    'error' => 'error::error',
                ]
            ],
        ]
    ],
    'middleware_pipeline' => [
        'error_normalizer' => [
            'middleware' => [
                Dnbk\ErrorHandling\NotFoundNormalizerMiddleware::class,
            ],
            'priority' => -1000,
        ],
        'error' => [
            'middleware' => [
                Dnbk\ErrorHandling\ErrorLoggerMiddleware::class,
                Dnbk\ErrorHandling\ExceptionProcessorMiddleware::class,
                Dnbk\ErrorHandling\JsonErrorHandler::class,
                Dnbk\ErrorHandling\HtmlErrorHandler::class,
            ],
            'error' => true,
            'priority' => -9999,
        ],
    ],
    'dependencies' => [
        'invokables' => [
            Dnbk\ErrorHandling\NotFoundNormalizerMiddleware::class => Dnbk\ErrorHandling\NotFoundNormalizerMiddleware::class,
            Dnbk\ErrorHandling\JsonErrorHandler::class => Dnbk\ErrorHandling\JsonErrorHandler::class,
        ],
        'factories' => [
            Dnbk\ErrorHandling\ErrorLoggerMiddleware::class => function ($c) {
                return new Dnbk\ErrorHandling\ErrorLoggerMiddleware($c->get('AppLogger'));
            },
            Dnbk\ErrorHandling\ExceptionProcessorMiddleware::class => function ($c) {
                return new Dnbk\ErrorHandling\ExceptionProcessorMiddleware($c['config']['error_handler']);
            },
            Dnbk\ErrorHandling\HtmlErrorHandler::class => function ($c) {
                return new Dnbk\ErrorHandling\HtmlErrorHandler(
                    $c->get('Zend\Expressive\Template\TemplateRendererInterface'),
                    $c['config']['error_handler']['handlers']['html']['templates'],
                    $c['config']['error_handler']['display_exceptions']
                );
            },
        ],
    ],
];
