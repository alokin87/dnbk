<?php

namespace Dnbk\Mail;

use Nette\Mail\IMailer;
use Nette\Mail\Message;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class Mailer implements IMailer
{
    /**
     * @var IMailer
     */
    private $mailer;

    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $fakeTo = null;

    public function __construct(IMailer $mailer, $from, $fakeTo = null)
    {
        $this->mailer = $mailer;
        $this->from = $from;
        $this->fakeTo = $fakeTo;
    }

    public function send(Message $mail)
    {
        $mail->setFrom($this->from);

        if (null !== $this->fakeTo) {
            $mail->setHeader('To', $this->fakeTo, false);
        }

        $this->mailer->send($mail);
    }
}
