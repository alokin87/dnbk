<?php

namespace Dnbk\Domain\Entity\Address;

use Dnbk\Util\Stringy;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class Municipality
{
    private $name;

    private $id = null;

    private function __construct()
    {
    }

    public static function create($name)
    {
        $municipality = new self();
        $municipality->name = $name;

        return $municipality;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        if (null === $this->id) {
            $this->id = (string) Stringy::create($this->name)->slugify();
        }

        return $this->id;
    }

    public function equals(Municipality $municipality)
    {
        return $municipality->getId() === $this->getId();
    }
}
