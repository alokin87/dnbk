<?php

namespace Dnbk\Tests\EdbPowerCuts;

use PHPUnit_Framework_TestCase as TestCase;
use Dnbk\Domain\EdbPowerCuts\Scraper;
use Dnbk\Domain\EdbPowerCuts\Schedule;
use DateTime;
use DateInterval;
use Dnbk\Domain\Entity\Address\Street;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class ScraperTest extends TestCase
{
    /**
     * @var Scraper
     */
    protected $scraper;

    public function setUp()
    {
        $this->scraper = new Scraper(function($url) {
            $url = str_replace(Scraper::BASE_URL, '', $url);
            $url = trim($url, '/');
            return file_get_contents('_files/' . $url);
        });
    }

    /**
     * @expectedException \Dnbk\Domain\EdbPowerCuts\Exception\ScraperException
     */
    public function testScrapingFailsForUnsupportedDay()
    {
        $this->scraper->getSchedule((new DateTime())->add(new DateInterval('P1M')));
    }

    public function testGetSchedule()
    {
        $today = new DateTime();

        $schedule = $this->scraper->getSchedule($today);

        $this->assertInstanceOf(Schedule::class, $schedule);

        $this->assertEquals($today->format('Y-m-d'), $schedule->getDate()->format('Y-m-d'));

        $this->assertInternalType('array', $schedule->getItems());
        $this->assertNotEmpty($schedule->getItems());
    }

    public function testGetScheduleFetchesCorrectList()
    {
        $today = new DateTime();

        $schedule = $this->scraper->getSchedule($today);

        $items = $schedule->findByStreet(Street::create('MAKENZIJEVA',  'Vračar'));
        $this->assertNotEmpty($items);
        $this->assertCount(1, $items);

        $scheduleItem = current($items);

        $this->assertEquals("MAKENZIJEVA", $scheduleItem->getStreet()->getName());
        $this->assertEquals("Vračar", $scheduleItem->getStreet()->getMunicipality()->getName());
        $this->assertEquals('01:30', $scheduleItem->getPowerCutStartDate()->format('H:i'));
        $this->assertEquals('04:30', $scheduleItem->getPowerCutEndDate()->format('H:i'));

        $items = $schedule->findByStreet(Street::create('VUKA KARADŽIĆA', 'Čukarica'));
        $this->assertCount(1, $items);

        $scheduleItem = current($items);
        $this->assertEquals("VUKA KARADŽIĆA", $scheduleItem->getStreet()->getName());
        $this->assertEquals("Čukarica", $scheduleItem->getStreet()->getMunicipality()->getName());
        $this->assertEquals('08:30', $scheduleItem->getPowerCutStartDate()->format('H:i'));
        $this->assertEquals('14:00', $scheduleItem->getPowerCutEndDate()->format('H:i'));
        $this->assertCount(3, $scheduleItem->getStreetNumberRanges());
        $this->assertEquals([4, 28], $scheduleItem->getStreetNumberRanges()[0]);
        $this->assertEquals([3, 5], $scheduleItem->getStreetNumberRanges()[1]);
        $this->assertEquals([9, 15], $scheduleItem->getStreetNumberRanges()[2]);
    }

    public function testGetScheduleAddsAltStreetNames()
    {
        $schedule = $this->scraper->getSchedule(new DateTime('+2 days'));

        $this->assertNotEmpty($schedule->findByStreet(Street::create('KNEZA MIHAILA',  'Stari grad')));
        $this->assertNotEmpty($schedule->findByStreet(Street::create('KNEZ MIHAILOVA',  'Stari grad')));
    }
}
