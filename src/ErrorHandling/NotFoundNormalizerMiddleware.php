<?php

namespace Dnbk\ErrorHandling;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Dnbk\Exception\PageNotFoundException;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class NotFoundNormalizerMiddleware
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        if ($response->getStatusCode() === 200 && $response->getBody()->getSize() === 0) { //404?
            throw new PageNotFoundException();
        }
    }
}
