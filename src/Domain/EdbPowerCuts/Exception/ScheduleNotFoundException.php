<?php

namespace Dnbk\Domain\EdbPowerCuts\Exception;

use DateTime;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class ScheduleNotFoundException extends RuntimeException
{
    public static function forDate(DateTime $date)
    {
        return new self("Power cuts schedule for day '{$date->format('Y-m-d')}' was not found");
    }
}
