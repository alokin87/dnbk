<?php

namespace Dnbk\Domain\Service;

use Dnbk\Domain\Repository\SubscriptionRepositoryInterface as SubscriptionRepository;
use Dnbk\Domain\Entity\Subscription;
use Dnbk\Domain\Entity\Contact;
use Dnbk\Domain\Exception;
use Dnbk\Domain\Service\ConfirmSubscriptionNotification;
use Dnbk\Container\ContainerAwareInterface;
use Dnbk\Container\ContainerAwareTrait;
use Dnbk\Helper\UrlHelper;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class SubscriptionService implements
    SubscriptionServiceInterface,
    ContainerAwareInterface,
    LoggerAwareInterface
{
    use ContainerAwareTrait;
    use LoggerAwareTrait;

    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepo;

    public function __construct(SubscriptionRepository $subscriptionRepo)
    {
        $this->subscriptionRepo = $subscriptionRepo;
    }

    public function subscribe(array $input)
    {
        if (!isset($input['address'])) {
            throw Exception\InvalidSubscriptionException::missingAddressStreet();
        }

        if (!isset($input['contact'])) {
            throw Exception\InvalidSubscriptionException::missingContact();
        }

        $address = $input['address'];

        $contact = Contact::createNew($input['contact']);

        if ($contact->isMobile()) {
            throw Exception\InvalidSubscriptionException::mobileNotSupported();
        }

        $subscription = Subscription::createNew($address, $contact);

        if (null !== $this->subscriptionRepo->find($subscription)) {
            throw Exception\DuplicateSubscriptionException::forSubscription($subscription);
        }

        $this->subscriptionRepo->save($subscription);

        $this->sendConfirmSubscriptionNotification($subscription);

        if (null !== $this->logger) {
            $this->logger->info('New subscription', ['subscription' => $subscription]);
        }

        return $subscription;
    }

    private function sendConfirmSubscriptionNotification(Subscription $subscription)
    {
        $notification = new ConfirmSubscriptionNotification(
            $subscription,
            $this->getContainer()->get(UrlHelper::class)->generate(
                'confirmSubscription',
                [],
                ['query' => ['c' => $subscription->getUid()], 'force_canonical' => true]
            )
        );
        $notification();
    }

    public function confirm($code)
    {
        $subscription = $this->subscriptionRepo->findByUid($code);

        if (null === $subscription) {
            throw Exception\InvalidSubscriptionCodeException::notExists();
        }

        if ($subscription->isActive()) {
            throw Exception\InvalidSubscriptionCodeException::alreadyActivated();
        }

        $subscription->confirm($code);

        $this->subscriptionRepo->save($subscription);
    }

    public function unsubscribe($code)
    {
        $subscription = $this->subscriptionRepo->findByUid($code);

        if (null === $subscription) {
            throw Exception\InvalidSubscriptionCodeException::notExists();
        }

        if (!$subscription->isActive()) {
            throw Exception\InvalidSubscriptionCodeException::inactiveSubscriptionRemoval();
        }

        $this->subscriptionRepo->remove($subscription);

        if (null !== $this->logger) {
            $this->logger->info('Unsubscribe', ['subscription' => $subscription]);
        }
    }
}
