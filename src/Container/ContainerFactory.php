<?php

namespace Dnbk\Container;

use Interop\Container\Pimple\PimpleInterop;
use Dnbk\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class ContainerFactory
{
    private function __construct()
    {
    }

    public static function create($config)
    {
        $container = new PimpleInterop();

        $container['config'] = $config;

        // Inject factories
        if (isset($config['dependencies']['factories'])) {
            foreach ($config['dependencies']['factories'] as $name => $factory) {
                $container[$name] = $container->share(function ($c) use ($factory) {
                    if (is_string($factory)) {
                        $factory = new $factory();
                    } elseif (!is_callable($factory)) {
                        throw new Exception\InvalidArgumentException('Factory should be either class name (string) or callable');
                    }

                    $object = $factory($c);

                    if ($object instanceof ContainerAwareInterface) {
                        $object->setContainer($c);
                    }

                    return $object;
                });
            }
        }

        // Inject invokables
        if (isset($config['dependencies']['invokables'])) {
            foreach ($config['dependencies']['invokables'] as $name => $className) {
                $container[$name] = $container->share(function ($c) use ($className) {
                    $object = new $className();

                    if ($object instanceof ContainerAwareInterface) {
                        $object->setContainer($c);
                    }

                    return $object;
                });
            }
        }

        return $container;
    }
}
