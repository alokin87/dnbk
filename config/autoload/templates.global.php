<?php

return [
    'templates' => [
        'extension' => 'phtml',
        'paths' => [
            'app'    => ['templates/app'],
            'layout' => ['templates/layout'],
            'error'  => ['templates/error'],
            'notification'  => ['templates/notification'],
        ],
        'assets' => 'public'
    ],

    'dependencies' => [
        'factories' => [
            Zend\Expressive\Template\TemplateRendererInterface::class =>
                Dnbk\Template\PlatesRendererFactory::class,
        ],
    ],
];
