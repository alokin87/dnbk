<?php

namespace Dnbk\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}
