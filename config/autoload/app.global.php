<?php

return [
    'middleware_pipeline' => [
        'always' => [
            'middleware' => [
                Dnbk\Helper\UrlHelperMiddleware::class,
            ],
            'priority' => PHP_INT_MAX,
        ],
        'routing' => [
            'middleware' => [
                Zend\Expressive\Container\ApplicationFactory::ROUTING_MIDDLEWARE,
                Zend\Expressive\Container\ApplicationFactory::DISPATCH_MIDDLEWARE,
            ],
            'priority' => 1,
        ],
    ],
    'dependencies' => [
        'invokables' => [
            Dnbk\Domain\Repository\SubscriptionRepositoryInterface::class => Dnbk\Domain\Repository\SubscriptionRepository::class,
            Dnbk\Domain\Repository\ContactRepositoryInterface::class => Dnbk\Domain\Repository\ContactRepository::class,
            Dnbk\Console\CrawlerCommand::class => Dnbk\Console\CrawlerCommand::class,
            Dnbk\Console\SubscriptionsCheckerCommand::class => Dnbk\Console\SubscriptionsCheckerCommand::class,
        ],
        'factories' => [
            Zend\Expressive\Application::class => Zend\Expressive\Container\ApplicationFactory::class,
            Dnbk\Cache\Manager::class => function($c) {
                return new Dnbk\Cache\Manager($c['config']['cache']);
            },
            Dnbk\Domain\EdbPowerCuts\ScraperInterface::class => function($c) {
                $cacheManager = $c->get(Dnbk\Cache\Manager::class);

                return new Dnbk\Domain\EdbPowerCuts\CachingScraper(
                    new Dnbk\Domain\EdbPowerCuts\Scraper(null, $c->get('CrawlerLogger')),
                    $cacheManager->get('edb_power_cuts_schedule'),
                    $cacheManager->getOptions('edb_power_cuts_schedule')['lifetime']
                );
            },
            Dnbk\Domain\EdbPowerCuts\ScheduleRepoInterface::class => function($c) {
                return new Dnbk\Domain\EdbPowerCuts\ScheduleRepo($c->get(Dnbk\Domain\EdbPowerCuts\ScraperInterface::class));
            },
            Dnbk\Domain\EdbPowerCuts\CheckerInterface::class => function($c) {
                return new Dnbk\Domain\EdbPowerCuts\Checker($c->get(Dnbk\Domain\EdbPowerCuts\ScheduleRepoInterface::class));
            },
            Dnbk\Domain\Service\CheckerServiceInterface::class => function($c) {
                return new Dnbk\Domain\Service\CheckerService(
                    $c->get(Dnbk\Domain\EdbPowerCuts\CheckerInterface::class),
                    $c->get(Dnbk\Domain\Repository\SubscriptionRepositoryInterface::class)
                );
            },
            Dnbk\Domain\Service\SubscriptionServiceInterface::class => function($c) {
                $service = new Dnbk\Domain\Service\SubscriptionService($c->get(Dnbk\Domain\Repository\SubscriptionRepositoryInterface::class));
                $service->setLogger($c->get('AppLogger'));
                return $service;
            },
            Dnbk\Helper\UrlHelper::class => function($c) {
                $urlHelper = new Dnbk\Helper\UrlHelper($c->get(Zend\Expressive\Router\RouterInterface::class));
                if (!empty($c['config']['base_url'])) {
                    $urlHelper->setBaseUrl($c['config']['base_url']);
                }
                return $urlHelper;
            },
            Dnbk\Helper\UrlHelperMiddleware::class => function($c) {
                return new Dnbk\Helper\UrlHelperMiddleware($c->get(Dnbk\Helper\UrlHelper::class));
            },
        ]
    ],

    'config_cache_enabled' => false,

    'cache' => [
        'edb_power_cuts_schedule' => [
            'adapter' => 'Filesystem',
            'options' => [
                'dir' => 'data/cache',
                'lifetime' => 3600
            ],
        ]
    ],
];
