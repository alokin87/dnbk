<?php

namespace Dnbk\Domain\Service;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
interface SubscriptionServiceInterface
{
    public function subscribe(array $input);

    public function confirm($code);

    public function unsubscribe($code);
}
