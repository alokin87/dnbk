<?php

namespace Dnbk\ErrorHandling;

use Zend\Stratigility\ErrorMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class HtmlErrorHandler implements ErrorMiddlewareInterface
{
    /**
     * @var TemplateRendererInterface
     */
    private $templateRenderer;

    /**
     * @var string
     */
    private $templates;

    /**
     * @var bool
     */
    private $displayExceptions = false;

    public function __construct(TemplateRendererInterface $templateRenderer, array $templates, $displayExceptions = false)
    {
        $this->templateRenderer = $templateRenderer;
        $this->templates = $templates;
        $this->displayExceptions = $displayExceptions;
    }

    public function __invoke($error, ServerRequestInterface $request, ResponseInterface $response, callable $out = null)
    {
        $status = $error->getCode();

        $data = [
            'error' => $error->getMessage(),
            'status' => $status,
        ];

        if ($this->displayExceptions) {
            $data['exception'] = [
                'type'    => get_class($error),
                'message' => $error->getMessage(),
                'file'    => $error->getFile(),
                'line'    => $error->getLine(),
                'trace'   => $error->getTraceAsString()
            ];
        }

        $template = isset($this->templates[$status])
            ? $this->templates[$status]
            : $this->templates['error'];

        $response->getBody()->write(
            $this->templateRenderer->render($template, $data)
        );

        return $response;
    }
}
