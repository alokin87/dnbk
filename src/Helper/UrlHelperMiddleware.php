<?php

namespace Dnbk\Helper;

use Dnbk\Helper\UrlHelper;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class UrlHelperMiddleware
{
    /**
     * @var UrlHelper
     */
    private $helper;

    public function __construct(UrlHelper $helper)
    {
        $this->helper = $helper;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $baseUrl = '';

        $uri = $request->getUri();
        if (($scheme = $uri->getScheme())) {
            $baseUrl .= sprintf('%s://', $scheme);
        }
        if (($authority = $uri->getAuthority())) {
            $baseUrl .= $authority;
        }

        $this->helper->setBaseUrl($baseUrl);

        return $next($request, $response);
    }
}
