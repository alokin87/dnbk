<?php

namespace Dnbk\ErrorHandling;

use Zend\Stratigility\ErrorMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Dnbk\Exception\ClientErrorInterface;
use Dnbk\Exception\PageNotFoundException;
use RuntimeException;
use Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class ExceptionProcessorMiddleware implements ErrorMiddlewareInterface
{
    /**
     * @var array
     */
    private $options;

    public function __construct(array $options)
    {
        $this->options = array_merge([
            'display_exceptions' => false,
        ], $options);
    }

    /**
     * {@inheritDoc}
     */
    public function __invoke($error, ServerRequestInterface $request, ResponseInterface $response, callable $out = null)
    {
        return $out($request, $response, $this->processError($error));
    }

    private function processError($error)
    {
        if (!$error instanceof Exception) {
            return new RuntimeException((string) $error, 500);
        }

        if ($error instanceof ClientErrorInterface) {
            switch (get_class($error)) {
                case PageNotFoundException::class :
                    $code = 404;
                    break;
                default :
                    $code = 400;
                    break;
            }

            return new RuntimeException($error->getMessage(), $code);
        }

        if (!$this->options['display_exceptions']) {
            return new RuntimeException('An error has occurred', 500);
        }

        return new RuntimeException($error->getMessage(), 500);
    }
}
