<?php

namespace Dnbk\Domain\EdbPowerCuts;

use DateTime;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
interface ScraperInterface
{
    /**
     * @param DateTime $date
     * @return Schedule
     */
    public function getSchedule(DateTime $date);
}
