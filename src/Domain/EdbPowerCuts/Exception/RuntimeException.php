<?php

namespace Dnbk\Domain\EdbPowerCuts\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}
