<?php

namespace Dnbk\Domain\EdbPowerCuts\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class ScraperException extends \RuntimeException implements ExceptionInterface
{
    public static function pageCantBeFetched($day)
    {
        return new self("Power cuts web page for the day '$day' cannot be fetched");
    }

    public static function pageCantBeParsed()
    {
        return new self('Power cuts web page cannot be parsed');
    }
}
