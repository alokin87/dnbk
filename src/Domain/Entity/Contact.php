<?php

namespace Dnbk\Domain\Entity;

use Illuminate\Database\Eloquent\Model;
use Dnbk\Domain\Exception;
use Notify\Contact\ContactInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class Contact extends Model implements ContactInterface
{
    const TYPE_EMAIL = 1;
    const TYPE_MOBILE = 2;

    /**
     * @var array
     */
    private static $validTypes = [
        self::TYPE_EMAIL => 'email',
        self::TYPE_MOBILE => 'mobile',
    ];

    protected $table = 'contacts';

    protected $primaryKey = 'id';

    public $incrementing = true;

    public $timestamps = false;

    protected $fillable = [];

    protected $casts = [
        'type' => 'int',
    ];

    /**
     * @param string|array $data
     * @return self
     */
    public static function createNew($data)
    {
        if (is_string($data)) {
            if (filter_var($data, FILTER_VALIDATE_EMAIL)) {
                $value = $data;
                $type = self::TYPE_EMAIL;
            } elseif (ctype_digit(str_replace(['+', '-', ' '], '', $data))) {
                $value = $data;
                $type = self::TYPE_MOBILE;
            } else {
                throw Exception\InvalidContactException::typeNotResolved();
            }
        } else {
            if (!is_array($data)) {
                throw new Exception\InvalidArgumentException('Contact should be either string or array');
            }

            if (!isset($data['type'])) {
                throw Exception\InvalidContactException::missingType();
            }

            if (!isset($data['value'])) {
                throw Exception\InvalidContactException::missingValue();
            }

            $type = $data['type'];
            $value = $data['value'];

            if (!isset(self::$validTypes[$type])) {
                if (false === ($typeId = array_search($type, self::$validTypes))) {
                    throw Exception\InvalidContactException::unsupportedType($type);
                }

                $type = $typeId;
            }

            switch ($type) {
                case self::TYPE_EMAIL :
                    if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                        throw Exception\InvalidContactException::invalidEmail();
                    }
                    break;
                default :
                    break;
            }
        }

        $contact = new self();
        $contact->forceFill([
            'type' => $type,
            'value' => $value
        ]);

        return $contact;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getKey();
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->getAttribute('type');
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->getAttribute('value');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getType();
    }

    /**
     * @return bool
     */
    public function isEmail()
    {
        return self::TYPE_EMAIL == $this->getType();
    }

    /**
     * @return bool
     */
    public function isMobile()
    {
        return self::TYPE_MOBILE == $this->getType();
    }
}
