<?php

namespace Dnbk\Domain\Service;

use Dnbk\Domain\Entity\Address\Address;
use Dnbk\Domain\EdbPowerCuts\ScheduleItem;
use DateTime;
use DatePeriod;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
interface CheckerServiceInterface
{
    /**
     * @param DatePeriod $period
     * @param $notify bool
     * @return SubscriptionCheckMatch[]
     */
    public function checkSubscriptions(DatePeriod $period = null, $notify = true);

    /**
     * @param array|Address $address
     * @param DatePeriod $period
     * @return ScheduleItem[]
     */
    public function checkForPeriod($address, DatePeriod $period = null);

    /**
     * @param array|Address $address
     * @param DateTime $date
     * @return ScheduleItem[]
     */
    public function checkForDate($address, DateTime $date);

    /**
     * @param array|Address $address
     * @param int $day
     * @return ScheduleItem[]
     */
    public function checkForDay($address, $day);
}
