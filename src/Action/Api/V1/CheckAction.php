<?php

namespace Dnbk\Action\Api\V1;

use Dnbk\Domain\Service\CheckerServiceInterface as CheckerService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Dnbk\Domain\Entity\Address\Address;
use Dnbk\Domain\Entity\Address\Street;
use DateTime;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class CheckAction
{
    /**
     * @var CheckerService
     */
    private $checkerService;

    public function __construct(CheckerService $checkerService)
    {
        $this->checkerService = $checkerService;
    }

    private function success($data)
    {
        return new JsonResponse([
            'status' => true,
            'response' => $data,
        ]);
    }

    private function error($message, $code = 500)
    {
        return new JsonResponse([
            'status' => false,
            'response' => $message,
        ], $code);
    }

    private function formatPowerCutPeriod($period)
    {
        $tz = new \DateTimeZone('Europe/Belgrade');

        $start = $period[0];
        $start = $start->setTimezone($tz);

        $end = $period[1];
        $end = $end->setTimezone($tz);

        return $start->format('H:i') . ' - ' . $end->format('H:i');
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $params = $request->getAttribute('params');

        if (empty($params)) {
            return $this->error('invalidRequest', 400);
        }

        $params = array_map('trim', $params);

        $day = $streetName = $streetNumber = $municipality = null;

        if (ctype_digit($params[0])) {
            $day = (int) $params[0];

            if (!isset($params[1])) {
                return $this->error('invalidRequest', 400);
            }

            $streetName = $params[1];

            if (isset($params[2])) {
                $streetNumber = $params[2];
            }

            if (isset($params[3])) {
                $municipality = $params[3];
            }
        } else {
            $streetName = $params[0];

            if (isset($params[1])) {
                $streetNumber = $params[1];
            }

            if (isset($params[2])) {
                $municipality = $params[2];
            }
        }

        $streetName = urldecode($streetName);

        $address = null;

        try {
            $address = Address::create(Street::create($streetName, $municipality), $streetNumber);

            if (null !== $day) {
                $items = $this->checkerService->checkForDay($address, $day);

                return $this->success(!empty($items) ? $this->formatPowerCutPeriod($items[0]->getPowerCutPeriod()) : false);
            }

            $items = $this->checkerService->checkForPeriod($address);

            $now = new DateTime();
            $now->setTime(0, 0, 0);

            $result = [
                0 => ['day' => 0, 'period' => null],
                1 => ['day' => 1, 'period' => null],
                2 => ['day' => 2, 'period' => null],
            ];
            foreach ($items as $item) {
                $scheduleDate = $item->getSchedule()->getDate();
                $scheduleDate = $scheduleDate->setTime(0, 0, 0);

                $interval = $now->diff($scheduleDate);
                $dayNum = $interval->days;

                $result[$dayNum] = [
                    'day' => $dayNum,
                    'period' => $this->formatPowerCutPeriod($item->getPowerCutPeriod())
                ];
            }

            $result = array_values($result);

            return $this->success($result);
        } catch (\Exception $ex) {
            return $this->error($ex->getMessage(), 500);
        }
    }
}
