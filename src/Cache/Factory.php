<?php

namespace Dnbk\Cache;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class Factory
{
    public function create($adapterName, array $options = [])
    {
        switch (strtolower($adapterName)) {
            case 'filesystem' :
                $adapter = new \Doctrine\Common\Cache\FilesystemCache($options['dir']);
                break;
            case 'chain' :
                $adapters = [];
                foreach ($options['adapters'] as $adapter) {
                    $adapters[] = $this->create($adapter['name'], isset($adapter['options']) ? $adapter['options'] : []);
                }
                $adapter = new \Doctrine\Common\Cache\ChainCache($adapters);
                break;
            default :
                $adapterClassName = "Doctrine\\Common\\Cache\\{$adapterName}Cache";
                $adapter = new $adapterClassName();
                break;
        }

        return $adapter;
    }
}
