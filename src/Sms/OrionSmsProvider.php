<?php

namespace Dnbk\Sms;

use GuzzleHttp\Client as HttpClient;
use Dnbk\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class OrionSmsProvider implements SmsProvider
{
    const BASE_URI = 'http://moj.oriontelekom.rs';

    const ENDPOINT_LOGIN = 'logindo.asp';
	const ENDPOINT_ACCOUNT = 'account/default.asp';
	const ENDPOINT_SMS_SEND = 'service/sms/default.asp?Action=Send';

    /**
	 * @var string
	 */
	private $username;

	/**
	 * @var string
	 */
	private $password;

    /**
     * @var string
     */
    private $fakeTo = null;

    /**
	 * @var HttpClient
	 */
	private $httpClient;

    /**
     * @var bool
     */
    private $loggedIn = false;

    public function __construct($username, $password, $fakeTo = null)
	{
		$this->username = $username;
		$this->password = $password;
        $this->fakeTo = $fakeTo;
	}

    private function sendRequest($method, $uri, array $options = [])
    {
        if (null === $this->httpClient) {
            $this->httpClient = new HttpClient([
                'base_uri' => self::BASE_URI,
                'cookies' => true,
                'timeout' => 10
            ]);
        }

        $response = $this->httpClient->request($method, $uri, $options);

        if (! ($response->getStatusCode() >= 200 && $response->getStatusCode() < 300)) {
            throw new Exception\RuntimeException("Orion SMS request '$uri' has failed; status: {$response->getStatusCode()}");
        }

        return $response;
    }

    private function login()
	{
		if (!$this->loggedIn) {
			$data = [
				'username'  => $this->username,
				'password'  => $this->password
			];

			$this->sendRequest('POST', self::ENDPOINT_LOGIN, ['form_params' => $data]);

			$this->loggedIn = true;
		}
	}

    private function isOverLimit()
	{
		$response = $this->sendRequest('GET', self::ENDPOINT_ACCOUNT);

        $matches = [];
        //<td\snowrap\sclass=\"tdl\"\snowrap>SMS\sporuke\s\[kom\]<\/td>
		preg_match_all('/<td\sclass=\"tdr\"\snowrap>([0-9]+)<\/td>/Ui', (string) $response->getBody(), $matches);

        $remaining = (int) $matches[1][1];

        return ($remaining <= 0);
	}

    public function send(Message $message)
    {
        $this->login();

		if ($this->isOverLimit()) {
			throw new Exception\RuntimeException("SMS messages limit has been exceeded");
		}

		$data = [
			'phone' => $this->fakeTo ?: $message->getTo(),
			'Msg'   => $message->getMessage()
		];

		$this->sendRequest('POST', self::ENDPOINT_SMS_SEND, ['form_params' => $data]);

		//'/<td\sclass=\"tdl\"\snowrap\svalign=\"top\"\sTITLE=\"[a-z0-9\sšđžčćŠĐŽČĆ]*\"><b>([a-z0-9\s]*)<\/b><\/td>/is';
    }
}
