<?php

error_reporting(E_ALL | E_STRICT);

date_default_timezone_set('Europe/Belgrade');

chdir(__DIR__);

require '../vendor/autoload.php';
