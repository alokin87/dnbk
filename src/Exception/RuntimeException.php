<?php

namespace Dnbk\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}
