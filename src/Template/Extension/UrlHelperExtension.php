<?php

namespace Dnbk\Template\Extension;

use League\Plates\Extension\ExtensionInterface;
use League\Plates\Engine;
use Dnbk\Helper\UrlHelper;

/**
 *
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class UrlHelperExtension implements ExtensionInterface
{
    /**
     * @var UrlHelper
     */
    private $urlHelper;

    public function __construct(UrlHelper $urlHelper)
    {
        $this->urlHelper = $urlHelper;
    }

    public function register(Engine $engine)
    {
        $engine->registerFunction('url', [$this, 'getUrl']);
    }

    public function getUrl($routeName, array $params = [])
    {
        return $this->urlHelper->generate($routeName, $params);
    }
}
