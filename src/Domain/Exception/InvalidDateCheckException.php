<?php

namespace Dnbk\Domain\Exception;

use RuntimeException;
use Dnbk\Exception\ClientErrorInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class InvalidDateCheckException extends RuntimeException implements
    ExceptionInterface,
    ClientErrorInterface
{
    public function __construct()
    {
        parent::__construct("Date param should be in future and no more than 3 days", ExceptionCodes::CODE_INVALID_DATE_CHECK);
    }
}
