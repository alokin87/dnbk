<?php

namespace Dnbk\Domain\EdbPowerCuts;

use DateTime;
use DatePeriod;

/**
 * Default schedule repo implementation.
 *
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class ScheduleRepo implements ScheduleRepoInterface
{
    /**
     * @var ScraperInterface
     */
    private $scraper;

    /**
     * @var array
     */
    private $schedules = [];

    /**
     * @var array
     */
    private $periodSchedules = [];

    public function __construct(ScraperInterface $scraper)
    {
        $this->scraper = $scraper;
    }

    public function find(DateTime $date)
    {
        $id = $date->format('Y-m-d');

        if (isset($this->schedules[$id])) {
            return $this->schedules[$id];
        }

        try {
            $this->schedules[$id] = $this->scraper->getSchedule($date);
        } catch (Exception\ScraperException $ex) {
            return null;
        }

        return $this->schedules[$id];
    }

    public function findForPeriod(DatePeriod $period)
    {
        $periodId = spl_object_hash($period);

        if (isset($this->periodSchedules[$periodId])) {
            return $this->periodSchedules[$periodId];
        }

        $schedules = [];

        foreach ($period as $date) {
            if (null !== ($schedule = $this->find($date))) {
                $schedules[] = $schedule;
            }
        }

        $this->periodSchedules[$periodId] = $schedules;

        return $schedules;
    }
}
