<?php

return [
    'email' => [
        'from' => 'danebudekasno@nikolaposa.in.rs'
    ],
    'sms' => [
        'provider' => [
            'orion' => [
                'username' => '',
                'password' => '',
            ]
        ],
    ],

    'dependencies' => [
        'factories' => [
            Nette\Mail\IMailer::class => function($c) {
                $config = $c['config']['email'];
                return new Dnbk\Mail\Mailer(
                    new Nette\Mail\SendmailMailer(),
                    $config['from'],
                    isset($config['fake_to']) ? $config['fake_to'] : null
                );
            },
            Dnbk\Sms\SmsProvider::class => function($c) {
                $config = $c['config']['sms'];
                $providerConfig = $config['provider']['orion'];
                return new Dnbk\Sms\OrionSmsProvider(
                    $providerConfig['username'],
                    $providerConfig['password'],
                    isset($config['fake_to']) ? $config['fake_to'] : null
                );
            },
            'Notification\Message\SendService\EmailSendService' => function($c) {
                return new Dnbk\Notification\Message\SendService\EmailSendService($c->get(Nette\Mail\IMailer::class));
            },
            'Notification\Message\SendService\SmsSendService' => function($c) {
                return new Dnbk\Notification\Message\SendService\SmsSendService($c->get(Dnbk\Sms\SmsProvider::class));
            },
            'Notify\Strategy\SendStrategy' => function($c) {
                return new Notify\Strategy\SendStrategy([
                    Notify\Message\EmailMessage::class => $c->get('Notification\Message\SendService\EmailSendService'),
                    Dnbk\Sms\Message::class => $c->get('Notification\Message\SendService\SmsSendService'),
                ]);
            },
        ],
    ],
];
