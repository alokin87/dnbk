<?php

namespace Dnbk\Domain\Service;

use Dnbk\Domain\Entity\Subscription;
use Dnbk\Domain\EdbPowerCuts\ScheduleItem as PowerCutsScheduleItem;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class SubscriptionCheckMatch
{
    /**
     * @var Subscription
     */
    private $subscription;

    /**
     * @var PowerCutsScheduleItem
     */
    private $powerCutsScheduleItem;

    public function __construct(Subscription $subscription, PowerCutsScheduleItem $powerCutsScheduleItem)
    {
        $this->subscription = $subscription;
        $this->powerCutsScheduleItem = $powerCutsScheduleItem;
    }

    public function getSubscription()
    {
        return $this->subscription;
    }

    public function getPowerCutsScheduleItem()
    {
        return $this->powerCutsScheduleItem;
    }

    public function toArray()
    {
        return [
            'subscription' => $this->getSubscription(),
            'schedule_item' => $this->getPowerCutsScheduleItem()
        ];
    }
}
