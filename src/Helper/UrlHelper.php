<?php

namespace Dnbk\Helper;

use Zend\Expressive\Router\RouterInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class UrlHelper
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = rtrim($baseUrl, '/');
    }

    /**
     * @param string $route
     * @param array $params
     * @param array $options
     * @return string
     */
    public function generate($route = null, array $params = [], array $options = [])
    {
        $uri = $this->router->generateUri($route, $params);

        if (!empty($options['query'])) {
            $uri .= '?' . http_build_query($options['query']);
        }

        if (isset($options['force_canonical']) && $options['force_canonical'] && null !== $this->baseUrl) {
            $uri = $this->baseUrl . '/' . ltrim($uri, '/');
        }

        return $uri;
    }

    /**
     * Proxies to generate().
     *
     * @param string $route
     * @param array $params
     * @param array $options
     * @return string
     */
    public function __invoke($route, array $params = [], array $options = [])
    {
        return $this->generate($route, $params, $options);
    }
}
