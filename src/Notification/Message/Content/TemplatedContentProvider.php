<?php

namespace Dnbk\Notification\Message\Content;

use Notify\Message\Content\ContentProviderInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Dnbk\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class TemplatedContentProvider implements ContentProviderInterface
{
    /**
     * @var string
     */
    private $template;

    /**
     * @var array
     */
    private $params;

    /**
     * @var TemplateRendererInterface
     */
    private $templateRenderer;

    /**
     * @var TemplateRendererInterface
     */
    private static $defaultTemplateRenderer;

    public function __construct($template, array $params, TemplateRendererInterface $templateRenderer = null)
    {
        $this->template = $template;
        $this->params = $params;

        if (null === $templateRenderer) {
            if (null === self::$defaultTemplateRenderer) {
                throw new Exception\LogicException('TemplatedContentProvider requires template renderer to be provided');
            }

            $templateRenderer = self::$defaultTemplateRenderer;
        }

        $this->templateRenderer = $templateRenderer;
    }

    public static function setDefaultTemplateRenderer(TemplateRendererInterface $defaultTemplateRenderer)
    {
        self::$defaultTemplateRenderer = $defaultTemplateRenderer;
    }

    public function getContent()
    {
        return $this->templateRenderer->render($this->template, $this->params);
    }
}
