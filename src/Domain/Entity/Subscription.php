<?php

namespace Dnbk\Domain\Entity;

use Illuminate\Database\Eloquent\Model;
use Dnbk\Domain\Entity\Address\Address;
use Dnbk\Domain\Entity\Address\Street;
use Dnbk\Domain\Exception;
use Ramsey\Uuid\Uuid;
use Notify\Message\Actor\ProvidesRecipientInterface;
use Notify\Message\Actor\Actor;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class Subscription extends Model implements ProvidesRecipientInterface
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @var Address
     */
    private $address;

    // ELOQUENT start

    protected $table = 'subscriptions';

    protected $primaryKey = 'id';

    public $incrementing = true;

    public $timestamps = false;

    protected $fillable = [];

    protected $casts = [
        'status' => 'int',
    ];

    protected $dates = [
        'created_at',
    ];

    public function contact()
    {
        return $this->belongsTo(Contact::class); //many to one
    }

    // ELOQUENT end

    public static function createNew($address, $contact)
    {
        if (is_array($address)) {
            try {
                $address = Address::createFromArray($address);
            } catch (Exception\ExceptionInterface $ex) {
                throw new Exception\InvalidSubscriptionException($ex->getMessage());
            }
        } elseif (!$address instanceof Address) {
            throw new Exception\InvalidArgumentException('Address should be either array or ' . Address::class . ' instance');
        }

        if (is_array($contact)) {
            try {
                $contact = Contact::createNew($contact);
            } catch (Exception\InvalidContactException $ex) {
                throw new Exception\InvalidSubscriptionException($ex->getMessage());
            }
        } elseif (!$contact instanceof Contact) {
            throw new Exception\InvalidArgumentException('Contact should be either array or ' . Contact::class . ' instance');
        }

        $subscription = new self();

        $subscription->forceFill([
            'uid' => (string) Uuid::uuid4(),
            'address_name' => $address->getStreet()->getName(),
            'address_municipality' => (null !== $address->getStreet()->getMunicipality())
                ? $address->getStreet()->getMunicipality()->getName()
                : null,
            'address_number' => $address->getStreetNumber(),
            'status' => self::STATUS_INACTIVE,
        ]);
        $subscription->setRelation('contact', $contact);

        return $subscription;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getKey();
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->getAttribute('uid');
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        if (null === $this->address) {
            $this->address = Address::create(
                Street::create(
                    $this->attributes['address_name'],
                    $this->attributes['address_municipality']
                ),
                $this->attributes['address_number']
            );
        }

        return $this->address;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->getRelationValue('contact');
    }

    /**
     * @param Contact $contact
     */
    public function setContact(Contact $contact)
    {
        $this->setRelation('contact', $contact);
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->getAttribute('status');
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->getAttribute('created_at');
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return self::STATUS_ACTIVE == $this->getStatus();
    }

    public function confirm($code)
    {
        if ($code !== $this->getUid()) {
            throw Exception\InvalidSubscriptionCodeException::mismatch();
        }

        $this->setAttribute('status', self::STATUS_ACTIVE);
    }

    public function jsonSerialize()
    {
        return [
            'address' => (string) $this->getAddress(),
            'contact' => [
                'type' => $this->getContact()->getType(),
                'value' => $this->getContact()->getValue(),
            ],
        ];
    }

    public function getMessageRecipient($messageType, $notificationId = null)
    {
        return new Actor($this->getContact());
    }
}
