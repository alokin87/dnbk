<?php

namespace Dnbk\Domain\Service;

use Notify\AbstractNotification;
use Dnbk\Domain\Entity\Subscription;
use Dnbk\Domain\EdbPowerCuts\ScheduleItem;
use DateTime;
use DateTimeZone;
use Dnbk\Util\Stringy;
use Notify\Message\EmailMessage;
use Dnbk\Sms\Message as SmsMessage;
use Notify\Message\Actor\Recipients;
use Dnbk\Notification\Message\Content\TemplatedContentProvider;
use Notify\Message\Options\Options;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class SubscriptionScheduledNotification extends AbstractNotification
{
    /**
     * @var Subscription
     */
    private $subscription;

    /**
     * @var ScheduleItem
     */
    private $scheduleItem;

    /**
     * @var string
     */
    private $unsubscribeUrl;

    public function __construct(Subscription $subscription, ScheduleItem $scheduleItem, $unsubscribeUrl = null)
    {
        $this->subscription = $subscription;
        $this->scheduleItem = $scheduleItem;
        $this->unsubscribeUrl = $unsubscribeUrl;
    }

    public function getName()
    {
        return 'Subscription scheduled';
    }

    public function getMessages()
    {
        $address = $this->subscription->getAddress();

        $tz = new DateTimeZone('Europe/Belgrade');
        $date = $this->scheduleItem->getSchedule()->getDate()->setTimezone($tz)->setTime(0, 0, 0);
        $periodStartDate = $this->scheduleItem->getPowerCutStartDate()->setTimezone($tz);
        $periodEndDate = $this->scheduleItem->getPowerCutEndDate()->setTimezone($tz);

        $dayInfo = $dateString = $date->format('d.m.Y');
        $relativeDayString = null;

        $interval = (new DateTime('now', $tz))->diff($date);
        switch ((int) $interval->days) {
            case 0 :
                $relativeDayString = 'danas';
                break;
            case 1 :
                $relativeDayString = 'sutra';
                break;
            case 2 :
                $relativeDayString = 'prekosutra';
                break;
            default :
                break;
        }

        if (null !== $relativeDayString) {
            $dayInfo .= ' (' . $relativeDayString . ')';
        }

        $periodString = $periodStartDate->format('H:i') . ' - ' . $periodEndDate->format('H:i');

        $subscriptionContact = $this->subscription->getContact();

        if ($subscriptionContact->isEmail()) {
            return [
                new EmailMessage(
                    Recipients::fromRecipientProviders([$this->subscription], EmailMessage::class),
                    '"Da ne bude kasno" - obaveštenje o planiranom isključenju električne energije',
                    new TemplatedContentProvider('notification::email/subscription_scheduled', [
                        'fullTitle' => '"Da ne bude kasno" - obaveštenje o planiranom isključenju električne energije',
                        'address' => (string) $address,
                        'day' => $dayInfo,
                        'period' => $periodString,
                        'scheduleUrl' => $this->scheduleItem->getSchedule()->getUrl(),
                        'unsubscribeUrl' => $this->unsubscribeUrl,
                    ]),
                    null,
                    new Options(['html' => true])
                )
            ];
        }

        if ($subscriptionContact->isMobile()) {
            return [
                new SmsMessage(
                    $this->subscription->getContact()->getValue(),
                    (new TemplatedContentProvider('notification::sms/subscription_scheduled', [
                        'address' => (string) Stringy::create($address->toShortString())->truncate(25, '...'),
                        'day' => $dateString,
                        'period' => $periodString,
                    ]))->getContent()
                )
            ];
        }

        return [];
    }
}
