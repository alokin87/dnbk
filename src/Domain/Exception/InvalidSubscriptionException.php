<?php

namespace Dnbk\Domain\Exception;

use RuntimeException;
use Dnbk\Exception\ClientErrorInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class InvalidSubscriptionException extends RuntimeException implements
    ExceptionInterface,
    ClientErrorInterface
{
    public static function missingAddressStreet()
    {
        return new self('Address street name must be supplied', ExceptionCodes::CODE_SUBSCRIPTION_MISSING_ADDRESS);
    }

    public static function missingContact()
    {
        return new self('Contact information be supplied', ExceptionCodes::CODE_SUBSCRIPTION_MISSING_CONTACT);
    }

    public static function mobileNotSupported()
    {
        return new self('Mobile subscription plan is not supported at the moment', ExceptionCodes::CODE_SUBSCRIPTION_MOBILE_NOT_SUPPORTED);
    }
}
