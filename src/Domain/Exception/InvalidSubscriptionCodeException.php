<?php

namespace Dnbk\Domain\Exception;

use RuntimeException;
use Dnbk\Exception\ClientErrorInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class InvalidSubscriptionCodeException extends RuntimeException implements
    ExceptionInterface,
    ClientErrorInterface
{
    public static function notExists()
    {
        return new self('Subscription code does not exist', ExceptionCodes::CODE_SUBSCRIPTION_NOT_EXISTS);
    }

    public static function mismatch()
    {
        return new self('Subscription code mismatch', ExceptionCodes::CODE_SUBSCRIPTION_MISMATCH);
    }

    public static function alreadyActivated()
    {
        return new self('Subscription has already been activated', ExceptionCodes::CODE_SUBSCRIPTION_ALREADY_ACTIVATED);
    }

    public static function inactiveSubscriptionRemoval()
    {
        return new self('Inactive subscription cannot be removed', ExceptionCodes::CODE_SUBSCRIPTION_INACTIVE_REMOVAL);
    }
}
