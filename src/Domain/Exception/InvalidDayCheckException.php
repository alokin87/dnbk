<?php

namespace Dnbk\Domain\Exception;

use RuntimeException;
use Dnbk\Exception\ClientErrorInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class InvalidDayCheckException extends RuntimeException implements
    ExceptionInterface,
    ClientErrorInterface
{
    public function __construct()
    {
        parent::__construct(
            "Day parameter can be either 0 (today), 1 (tomorrow), 2 (day after tomorrow) or 3 (3 days after)",
            ExceptionCodes::CODE_INVALID_DAY_CHECK
        );
    }
}
