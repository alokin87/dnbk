<?php

namespace Dnbk\Action\Exception;

use RuntimeException;
use Dnbk\Exception\ClientErrorInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class MissingParamException extends RuntimeException implements
    ExceptionInterface,
    ClientErrorInterface
{
    public static function param($name)
    {
        if (is_array($name)) {
            $names = "'" . implode("', '", $names) . "'";

            return new self(sprintf("%s parameters must be supplied", $names));
        }

        return new self(sprintf("%s parameter must be supplied", "'" .$name . "'"));
    }
}
