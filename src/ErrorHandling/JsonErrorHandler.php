<?php

namespace Dnbk\ErrorHandling;

use Zend\Stratigility\ErrorMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\JsonResponse;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class JsonErrorHandler implements ErrorMiddlewareInterface
{
    public function __invoke($error, ServerRequestInterface $request, ResponseInterface $response, callable $out = null)
    {
        $serverParams = $request->getServerParams();

        if (empty($serverParams['HTTP_X_REQUESTED_WITH']) || strtolower($serverParams['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
            return $out($request, $response, $error);
        }

        return new JsonResponse([
            'error' => $error->getMessage(),
            'code'  => $error->getCode(),
        ], (int) $error->getCode());
    }
}
