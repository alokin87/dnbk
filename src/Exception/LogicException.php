<?php

namespace Dnbk\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class LogicException extends \LogicException implements ExceptionInterface
{

}
