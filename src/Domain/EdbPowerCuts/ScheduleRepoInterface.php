<?php

namespace Dnbk\Domain\EdbPowerCuts;

use DateTime;
use DatePeriod;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
interface ScheduleRepoInterface
{
    /**
     * @param DateTime $date
     * @return Schedule
     */
    public function find(DateTime $date);

    /**
     * @param DatePeriod $period
     * @return Schedule[]
     */
    public function findForPeriod(DatePeriod $period);
}
