<?php

namespace Dnbk\Domain\Service;

use Notify\AbstractNotification;
use Dnbk\Domain\Entity\Subscription;
use Notify\Message\EmailMessage;
use Dnbk\Sms\Message as SmsMessage;
use Notify\Message\Actor\Recipients;
use Dnbk\Notification\Message\Content\TemplatedContentProvider;
use Notify\Message\Options\Options;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class ConfirmSubscriptionNotification extends AbstractNotification
{
    /**
     * @var Subscription
     */
    private $subscription;

    /**
     * @var string
     */
    private $confirmationUrl;

    public function __construct(Subscription $subscription, $confirmationUrl)
    {
        $this->subscription = $subscription;
        $this->confirmationUrl = $confirmationUrl;
    }

    public function getName()
    {
        return 'Confirm subscription';
    }

    public function getMessages()
    {
        $subscriptionContact = $this->subscription->getContact();

        if ($subscriptionContact->isEmail()) {
            return [
                new EmailMessage(
                    Recipients::fromRecipientProviders([$this->subscription], EmailMessage::class),
                    '"Da ne bude kasno" - Potvrda prijave',
                    new TemplatedContentProvider('notification::email/confirm_subscription', [
                        'fullTitle' => '"Da ne bude kasno" - Potvrda prijave',
                        'address' => (string) $this->subscription->getAddress(),
                        'confirmationUrl' => $this->confirmationUrl
                    ]),
                    null,
                    new Options(['html' => true])
                )
            ];
        }

        if ($subscriptionContact->isMobile()) {
            return [
                new SmsMessage(
                    $this->subscription->getContact()->getValue(),
                    (new TemplatedContentProvider('notification::sms/confirm_subscription', [
                        'confirmationUrl' => $this->confirmationUrl
                    ]))->getContent()
                )
            ];
        }

        return [];


    }
}
