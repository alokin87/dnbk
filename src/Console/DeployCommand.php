<?php

namespace Dnbk\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ZipArchive;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class DeployCommand extends Command
{
    const LATEST_RELEASE_URL = 'https://bitbucket.org/alokin87/dnbk/get/master.zip';

    const APP_ARCHIVE = 'data/tmp/dnbk.zip';
    const APP_DIR = 'data/tmp/dnbk';

    private $buildDir;

    private $appArchive;

    private $appDir;

    protected function configure()
    {
        $this->setName('deploy');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->buildDir = 'data/tmp/build';
        $this->appArchive = 'data/tmp/dnbk.zip';
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->download($output);

        $this->extract($output);

        $this->install($output);
    }

    private function download(OutputInterface $output)
    {
        $output->writeln('Downloading repo');

        file_put_contents($this->appArchive, fopen(self::LATEST_RELEASE_URL, 'r'));
    }

    private function extract(OutputInterface $output)
    {
        $output->writeln('Extracting repository archive');

        $zip = new ZipArchive();

        $zip->open($this->appArchive);
        $zip->extractTo($this->buildDir);

        $zip->close();
        @ unlink($this->appArchive);

        $archiveDirs = scandir($this->buildDir);
        $this->appDir = $this->buildDir . '/dnbk';
        rename($this->buildDir . DIRECTORY_SEPARATOR . $archiveDirs[2], $this->appDir);
    }

    private function install(OutputInterface $output)
    {
        chdir($this->appDir);

        $output->writeln('Installing vendor dependencies');

        system('composer install --no-dev --optimize-autoloader --no-progress 2>&1');
    }
}
