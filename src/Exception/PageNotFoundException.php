<?php

namespace Dnbk\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class PageNotFoundException extends \RuntimeException implements
    ExceptionInterface,
    ClientErrorInterface
{
}
