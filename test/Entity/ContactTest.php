<?php

namespace Dnbk\Tests\Entity;

use PHPUnit_Framework_TestCase as TestCase;
use Dnbk\Domain\Entity\Contact;
use Dnbk\Domain\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class ContactTest extends TestCase
{
    public function testCreationFailsIfDataTypeIsNotValid()
    {
        $this->setExpectedException(
            Exception\InvalidArgumentException::class,
            'should be either string or array'
        );

        Contact::createNew(false);
    }

    public function testCreationFromArray()
    {
        $contact = Contact::createNew([
            'type' => Contact::TYPE_MOBILE,
            'value' => '+38164111111'
        ]);

        $this->assertInstanceOf(Contact::class, $contact);
        $this->assertEquals(Contact::TYPE_MOBILE, $contact->getType());
        $this->assertEquals('+38164111111', $contact->getValue());
    }

    public function testCreatingEmailContactFromString()
    {
        $contact = Contact::createNew('test@example.com');

        $this->assertInstanceOf(Contact::class, $contact);
        $this->assertTrue($contact->isEmail());
        $this->assertEquals('test@example.com', $contact->getValue());
    }

    public function testCreatingMobileContactFromString()
    {
        $contact = Contact::createNew('111222');

        $this->assertInstanceOf(Contact::class, $contact);
        $this->assertTrue($contact->isMobile());
        $this->assertEquals('111222', $contact->getValue());
    }

    public function testCreationFailsIfTypeCannotBeResolved()
    {
        $this->setExpectedException(
            Exception\InvalidContactException::class,
            'Contact type was not supplied and it cannot be resolved'
        );

        Contact::createNew('foobar');
    }

    public function testCreationFailsIfTypeIsMissingInDataArray()
    {
        $this->setExpectedException(
            Exception\InvalidContactException::class,
            'type must be supplied'
        );

        Contact::createNew(['value' => 'test']);
    }

    public function testCreationFailsIfValueIsMissingInDataArray()
    {
        $this->setExpectedException(
            Exception\InvalidContactException::class,
            'value must be supplied'
        );

        Contact::createNew(['type' => 'test']);
    }

    public function testCreationFailsIfTypeIsNotSupported()
    {
        $this->setExpectedException(
            Exception\InvalidContactException::class,
            "'unsupported' is not supported"
        );

        Contact::createNew(['type' => 'unsupported', 'value' => 'test']);
    }

    public function testCreationFailsIfEmailIsNotValid()
    {
        $this->setExpectedException(
            Exception\InvalidContactException::class,
            'email address is not valid'
        );

        Contact::createNew(['type' => Contact::TYPE_EMAIL, 'value' => 'invalid_email']);
    }
}
