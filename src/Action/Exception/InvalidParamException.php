<?php

namespace Dnbk\Action\Exception;

use RuntimeException;
use Dnbk\Exception\ClientErrorInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class InvalidParamException extends RuntimeException implements
    ExceptionInterface,
    ClientErrorInterface
{
    public static function required($name)
    {
        return new self(sprintf("%s parameter is required and it cannot be empty", $name));
    }

    public static function invalidType($name, $type)
    {
        return new self(sprintf("%s parameter is not valid; should be %s", $name, $type));
    }
}
