<?php

namespace Dnbk\Domain\Service;

use Dnbk\Domain\EdbPowerCuts\CheckerInterface as EdbPowerCutsChecker;
use Dnbk\Domain\Repository\SubscriptionRepositoryInterface as SubscriptionRepository;
use Dnbk\Domain\Entity\Address\Address;
use DatePeriod;
use DateTime;
use DateInterval;
use Dnbk\Domain\Exception;
use Dnbk\Container\ContainerAwareInterface;
use Dnbk\Container\ContainerAwareTrait;
use Dnbk\Helper\UrlHelper;
use Dnbk\Domain\EdbPowerCuts\Exception\ScheduleNotFoundException;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class CheckerService implements
    CheckerServiceInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var EdbPowerCutsChecker
     */
    private $checker;

    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepo;

    public function __construct(EdbPowerCutsChecker $checker, SubscriptionRepository $subscriptionRepo)
    {
        $this->checker = $checker;
        $this->subscriptionRepo = $subscriptionRepo;
    }

    private static function getDefaultPeriod()
    {
        return new DatePeriod(new DateTime(), new DateInterval('P1D'), 3);
    }

    public function checkSubscriptions(DatePeriod $period = null, $notify = true)
    {
        if (null === $period) {
            $period = self::getDefaultPeriod();
        }

        $matches = [];

        $subscriptions = $this->subscriptionRepo->findActive();

        foreach ($subscriptions as $subscription) {
            $scheduleItems = $this->checker->checkForPeriod($subscription->getAddress(), $period);

            if (!empty($scheduleItems)) {
                foreach ($scheduleItems as $scheduleItem) {
                    $matches[] = new SubscriptionCheckMatch($subscription, $scheduleItem);
                }
            }
        }

        if ($notify) {
            foreach ($matches as $match) {
                $this->notify($match);
            }
        }

        return $matches;
    }

    private function notify(SubscriptionCheckMatch $match)
    {
        $unsubscribeUrl = null;
        if ('' !== (string) ($uid = $match->getSubscription()->getUid())) {
            $unsubscribeUrl = $this->getContainer()->get(UrlHelper::class)->generate(
                'unsubscribe',
                [],
                ['query' => ['c' => $uid], 'force_canonical' => true]
            );
        }

        $notification = new SubscriptionScheduledNotification(
            $match->getSubscription(),
            $match->getPowerCutsScheduleItem(),
            $unsubscribeUrl
        );
        $notification();
    }

    public function checkForPeriod($address, DatePeriod $period = null)
    {
        if (is_array($address)) {
            $address = Address::createFromArray($address);
        } elseif (!$address instanceof Address) {
            throw new Exception\InvalidAddressException('Address should be either array or Address instance');
        }

        if (null === $period) {
            $period = self::getDefaultPeriod();
        }

        return $this->checker->checkForPeriod($address, $period);
    }

    public function checkForDate($address, DateTime $date)
    {
        if (is_array($address)) {
            $address = Address::createFromArray($address);
        } elseif (!$address instanceof Address) {
            throw new Exception\InvalidAddressException('Address should be either array or Address instance');
        }

        try {
            return $this->checker->checkForDate($address, $date);
        } catch (ScheduleNotFoundException $ex) {
            throw new Exception\InvalidDateCheckException();
        }
    }

    public function checkForDay($address, $day)
    {
        if (is_array($address)) {
            $address = Address::createFromArray($address);
        } elseif (!$address instanceof Address) {
            throw new Exception\InvalidAddressException('Address should be either array or Address instance');
        }

        $day = (int) $day;

        try {
            return $this->checker->checkForDate($address, (new DateTime())->add(new DateInterval("P{$day}D")));
        } catch (ScheduleNotFoundException $ex) {
            throw new Exception\InvalidDayCheckException();
        }
    }
}
