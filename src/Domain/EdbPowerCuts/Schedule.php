<?php

namespace Dnbk\Domain\EdbPowerCuts;

use DateTimeImmutable;
use DateTime;
use Dnbk\Domain\Entity\Address\Street;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class Schedule
{
    /**
     * @var DateTimeImmutable
     */
    private $date;

    /**
     * @var ScheduleItem[]
     */
    private $items;

    /**
     * @var string
     */
    private $url;

    private function __construct()
    {
    }

    public static function create($date, array $items, $url = null)
    {
        $schedule = new self();

        if ($date instanceof DateTime) {
            $date = DateTimeImmutable::createFromFormat(DateTime::ISO8601, $date->format(DateTime::ISO8601), $date->getTimezone());
        } elseif (is_string($date)) {
            $date = new DateTimeImmutable($date);
        } elseif (! $date instanceof \DateTimeInterface) {
            throw new Exception\InvalidArgumentException('Schedule date should be either DateTime instance or string');
        }

        $schedule->date = $date;

        foreach ($items as $item) {
            if (!$item instanceof ScheduleItem) {
                throw new Exception\InvalidArgumentException('Invalid schedule item has been supplied');
            }
            $item->setSchedule($schedule);
        }

        $schedule->items = $items;

        $schedule->url = $url;

        return $schedule;
    }

    public static function createEmpty($date)
    {
        return self::create($date, []);
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function findByStreet(Street $street)
    {
        $schedule = [];

        foreach ($this->getItems() as $item) {
            if ($item->getStreet()->equals($street)) {
                $schedule[] = $item;
            }
        }

        return $schedule;
    }
}
