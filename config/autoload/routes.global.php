<?php

return [
    'routes' => [
        [
            'name' => 'home',
            'path' => '/',
            'middleware' => Dnbk\Action\HomePageAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'apiCheck',
            'path' => '/api/schedules/items',
            'middleware' => Dnbk\Action\Api\CheckAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'apiCheckSchedule',
            'path' => '/api/schedules/{day}/items',
            'middleware' => Dnbk\Action\Api\CheckAction::class,
            'allowed_methods' => ['GET', 'POST'],
            'options' => [
                'tokens' => [
                    'day' => '\d+',
                ],
            ],
        ],
        [
            'name' => 'apiSubscribe',
            'path' => '/api/subscriptions',
            'middleware' => Dnbk\Action\Api\SubscribeAction::class,
            'allowed_methods' => ['POST'],
        ],
        [
            'name' => 'confirmSubscription',
            'path' => '/subscribe/confirm',
            'middleware' => Dnbk\Action\ConfirmSubscriptionAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'unsubscribe',
            'path' => '/unsubscribe',
            'middleware' => Dnbk\Action\UnsubscribeAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'contact',
            'path' => '/contact',
            'middleware' => Dnbk\Action\ContactAction::class,
            'allowed_methods' => ['POST'],
        ],
        [
            'name' => 'apiCheckDeprecated',
            'path' => '/servis/provera',
            'middleware' => Dnbk\Action\Api\V1\CheckAction::class,
            'allowed_methods' => ['GET'],
            'options' => [
                'wildcard' => 'params',
            ],
        ],
    ],

    'dependencies' => [
        'invokables' => [
            Zend\Expressive\Router\RouterInterface::class => Dnbk\Router\AuraRouter::class,
        ],
		'factories' => [
            Dnbk\Action\HomePageAction::class => function($c) {
                return new Dnbk\Action\HomePageAction($c->get(Zend\Expressive\Template\TemplateRendererInterface::class));
            },
            Dnbk\Action\Api\CheckAction::class => function($c) {
                return new Dnbk\Action\Api\CheckAction($c->get(Dnbk\Domain\Service\CheckerServiceInterface::class));
            },
            Dnbk\Action\Api\SubscribeAction::class => function($c) {
                return new Dnbk\Action\Api\SubscribeAction($c->get(Dnbk\Domain\Service\SubscriptionServiceInterface::class));
            },
            Dnbk\Action\ConfirmSubscriptionAction::class => function($c) {
                return new Dnbk\Action\ConfirmSubscriptionAction(
                    $c->get(Dnbk\Domain\Service\SubscriptionServiceInterface::class),
                    $c->get(Zend\Expressive\Template\TemplateRendererInterface::class)
                );
            },
            Dnbk\Action\UnsubscribeAction::class => function($c) {
                return new Dnbk\Action\UnsubscribeAction(
                    $c->get(Dnbk\Domain\Service\SubscriptionServiceInterface::class),
                    $c->get(Zend\Expressive\Template\TemplateRendererInterface::class)
                );
            },
            Dnbk\Action\ContactAction::class => function($c) {
                return new Dnbk\Action\ContactAction($c->get(Nette\Mail\IMailer::class), $c['config']['contact']);
            },
            Dnbk\Action\Api\V1\CheckAction::class => function($c) {
                return new Dnbk\Action\Api\V1\CheckAction($c->get(Dnbk\Domain\Service\CheckerServiceInterface::class));
            },
        ]
    ],
];
