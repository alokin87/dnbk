<?php

namespace Dnbk\Domain\EdbPowerCuts;

use Dnbk\Domain\Entity\Address\Street;
use DateTimeImmutable;
use JsonSerializable;

/**
 * @SWG\Definition(
 *  definition="ScheduleItem",
 *  required={"street", "number_ranges", "day", "period", "more_info"},
 *  @SWG\Property(
 *      property="street",
 *      type="string"
 *  ),
 *  @SWG\Property(
 *      property="number_ranges",
 *      type="array",
 *      @SWG\Items(
 *          type="array",
 *          @SWG\Items(type="integer")
 *      )
 *  ),
 *  @SWG\Property(
 *      property="day",
 *      type="string",
 *      format="date"
 *  ),
 *  @SWG\Property(
 *      property="period",
 *      required={"start", "end"},
 *      properties={
 *          @SWG\Property(
 *              property="start",
 *              type="string",
 *              format="date-time"
 *          ),
 *          @SWG\Property(
 *              property="end",
 *              type="string",
 *              format="date-time"
 *          )
 *      }
 *  ),
 *  @SWG\Property(
 *      property="more_info",
 *      type="string"
 *  )
 * )
 *
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class ScheduleItem implements JsonSerializable
{
    /**
     * @var Street
     */
    private $street;

    /**
     * @var array
     */
    private $streetNumberRanges;

    /**
     * @var array
     */
    private $powerCutPeriod;

    /**
     * @var Schedule
     */
    private $schedule;

    private function __construct()
    {
    }

    public static function create(Street $street, $streetNumberRanges, $powerCutPeriod)
    {
        if (is_string($streetNumberRanges)) {
            $ranges = explode(',', $streetNumberRanges);

            $streetNumberRanges = [];

            foreach ($ranges as $rangeString) {
                $range = [];

                $numbers = explode('-', $rangeString);
                $range[] = (int) $numbers[0];
                if (isset($numbers[1])) {
                    $range[] = (int) $numbers[1];
                }

                $streetNumberRanges[] = $range;
            }
        } elseif (!is_array($streetNumberRanges)) {
            throw new Exception\InvalidArgumentException('Street numbers range should be either array or string');
        }

        if (!is_array($powerCutPeriod)) {
            throw new Exception\InvalidArgumentException('Power cut period should be either array or string');
        }
        if (count($powerCutPeriod) != 2) {
            throw new Exception\InvalidArgumentException('Power cut period must contain start and end date');
        }
        if (!$powerCutPeriod[0] instanceof DateTimeImmutable) {
            $powerCutPeriod[0] = new DateTimeImmutable($powerCutPeriod[0]);
        }
        if (!$powerCutPeriod[1] instanceof DateTimeImmutable) {
            $powerCutPeriod[1] = new DateTimeImmutable($powerCutPeriod[1]);
        }

        $scheduleItem = new self();

        $scheduleItem->street = $street;
        $scheduleItem->streetNumberRanges = $streetNumberRanges;
        $scheduleItem->powerCutPeriod = $powerCutPeriod;

        return $scheduleItem;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getStreetNumberRanges()
    {
        return $this->streetNumberRanges;
    }

    public function getPowerCutPeriod()
    {
        return $this->powerCutPeriod;
    }

    public function getPowerCutStartDate()
    {
        return $this->powerCutPeriod[0];
    }

    public function getPowerCutEndDate()
    {
        return $this->powerCutPeriod[1];
    }

    public function setSchedule(Schedule $schedule)
    {
        $this->schedule = $schedule;
    }

    public function getSchedule()
    {
        return $this->schedule;
    }

    public function jsonSerialize()
    {
        return [
            'street' => (string) $this->getStreet(),
            'number_ranges' => $this->getStreetNumberRanges(),
            'day' => $this->getSchedule()->getDate()->format('Y-m-d'),
            'period' => [
                'start' => $this->getPowerCutStartDate()->format('c'),
                'end' => $this->getPowerCutEndDate()->format('c'),
            ],
            'more_info' => $this->getSchedule()->getUrl(),
        ];
    }
}
