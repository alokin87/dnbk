<?php

namespace Dnbk\Domain\Exception;

use RuntimeException;
use Dnbk\Exception\ClientErrorInterface;
use Dnbk\Domain\Entity\Subscription;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class DuplicateSubscriptionException extends RuntimeException implements
    ExceptionInterface,
    ClientErrorInterface
{
    public static function forSubscription(Subscription $subscription)
    {
        return new self(sprintf(
            'Subscription for address: %s and contact: %s already exists',
            $subscription->getAddress(),
            $subscription->getContact()->getValue()
        ), ExceptionCodes::CODE_DUPLICATE_SUBSCRIPTION);
    }
}
