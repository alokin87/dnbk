<?php

namespace Dnbk\Domain\EdbPowerCuts;

use DateTime;
use DatePeriod;
use Dnbk\Domain\Entity\Address\Address;
use Dnbk\Domain\Entity\Address\Street;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class Checker implements CheckerInterface
{
    /**
     * @var ScheduleRepoInterface
     */
    private $scheduleRepo;

    /**
     * @var array
     */
    private $options;

    public function __construct(ScheduleRepoInterface $scheduleRepo, array $options = [])
    {
        $this->scheduleRepo = $scheduleRepo;

        $this->options = array_merge(
            [
                'street_compare_strategy' => self::class . "::defaultStreetCheck"
            ],
            $options
        );
    }

    public static function defaultStreetCheck(Street $scheduleItemStreet, Street $street)
    {
        return $scheduleItemStreet->like($street, 96);
    }

    private function doCheck(Address $address, Schedule $schedule)
    {
        $addressStreet = $address->getStreet();

        $streetCompareStrategy = $this->options['street_compare_strategy'];

        $now = new \DateTime();

        $scheduleItems = [];

        foreach ($schedule->getItems() as $scheduleItem) {
            if ($now >= $scheduleItem->getPowerCutStartDate()) { //ScheduleItem already started?
                continue;
            }

            if (call_user_func($streetCompareStrategy, $scheduleItem->getStreet(), $addressStreet)) {
                foreach ($scheduleItem->getStreetNumberRanges() as $range) {
                    if ($address->isInRange($range[0], isset($range[1]) ? $range[1] : $range[0])) {
                        $scheduleItems[] = $scheduleItem;
                    }
                }
            }
        }

        return $scheduleItems;
    }

    /**
     * @param Address $address
     * @param DateTime $date
     * @return ScheduleItem[]
     */
    public function checkForDate(Address $address, DateTime $date)
    {
        $schedule = $this->scheduleRepo->find($date);

        if (null === $schedule) {
            throw Exception\ScheduleNotFoundException::forDate($date);
        }

        return $this->doCheck($address, $schedule);
    }

    /**
     * @param Address $address
     * @param DatePeriod $period
     * @return ScheduleItem[]
     */
    public function checkForPeriod(Address $address, DatePeriod $period)
    {
        $scheduleItems = [];

        $schedules = $this->scheduleRepo->findForPeriod($period);

        foreach ($schedules as $schedule) {
            $scheduleItems = array_merge($scheduleItems, $this->doCheck($address, $schedule));
        }

        return $scheduleItems;
    }
}
