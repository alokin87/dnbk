#!/usr/bin/env php
<?php

/** @var \Interop\Container\ContainerInterface $container */
$container = require __DIR__ . '/../src/bootstrap.php';

$container->get('Zend\Expressive\Application');

$container->get(Dnbk\ErrorHandler\ErrorHandlerInterface::class)
    ->pushHandler(new Whoops\Handler\PlainTextHandler());

$application = new Symfony\Component\Console\Application('DNBK', '2.0.0');
$application->setCatchExceptions(false);
$application->add($container->get(Dnbk\Console\CrawlerCommand::class)); //45 * * * * php bin/app.php cron:crawler >/dev/null 2>&1
$application->add($container->get(Dnbk\Console\SubscriptionsCheckerCommand::class)); //0 18 * * * php bin/app.php cron:subscriptions_checker >/dev/null 2>&1
$application->add(new Dnbk\Console\DeployCommand());
$application->run();
