<?php

namespace Dnbk\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class DomainException extends \DomainException implements ExceptionInterface
{
}
