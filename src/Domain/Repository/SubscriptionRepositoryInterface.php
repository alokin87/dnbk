<?php

namespace Dnbk\Domain\Repository;

use Dnbk\Domain\Entity\Subscription;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
interface SubscriptionRepositoryInterface
{
    /**
     * @return Subscription[]
     */
    public function findAll();

    /**
     * @return Subscription[]
     */
    public function findActive();

    /**
     * @return Subscription|null
     */
    public function findByUid($uid);

    /**
     * @param Subscription $subscription
     * @return Subscription|null
     */
    public function find(Subscription $subscription);

    /**
     * @param Subscription $subscription
     * @return void
     */
    public function save(Subscription $subscription);

    /**
     * @param Subscription $subscription
     * @return void
     */
    public function remove(Subscription $subscription);
}
