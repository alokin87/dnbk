var MessageRenderer = function(container, templates) {
    this.$container = $(container);
    this.templates = (typeof templates === "undefined") ? [] : templates;
};
MessageRenderer.prototype.render = function(message, autohide) {
    this.$container.html(message);

    if (typeof autohide !== "undefined" && autohide === true) {
        this.$container.children().show().delay(10000).slideUp(800, function() {
            $(this).remove();
        });
    }
};
MessageRenderer.prototype.renderTemplate = function(name, data, autohide) {
    var template = this.templates[name];
    if (template) {
        this.$container.html(template(data));

        if (typeof autohide !== "undefined" && autohide === true) {
            this.$container.children().show().delay(10000).slideUp(800, function() {
                $(this).remove();
            });
        }
    }
};
MessageRenderer.prototype.clear = function() {
    this.$container.html("");
};

_.template.dateformat = function (datetime) {
    var d = new Date(datetime),
        fragments = [
            d.getDate(),
            d.getMonth() + 1,
            d.getFullYear()
        ];
    return fragments.join(".");
};
_.template.hourformat = function (datetime) {
    var d = new Date(datetime),
        fragments = [
            d.getHours() < 10 ? "0" + d.getHours().toString() : d.getHours().toString(),
            d.getMinutes() < 10 ? "0" + d.getMinutes().toString() : d.getMinutes().toString()
        ];
    return fragments.join(":");
};

var checkModule = (function ($, _, MessageRenderer) {
    var STREET_NAMES_REPO = "data/street_names.json";

    var $form = $("#check form");
    var $result = $("#check [role=result]");

    var actions = $form.data("actions");
    var action = null;

    var $formButton = null;

    var msgRenderer = new MessageRenderer($result, {
        "noMatches" : _.template("<div class=\"alert alert-success alert-dismissible\" role=\"alert\"> \
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Zatvori\"><span aria-hidden=\"true\">&times;</span></button> \
            <span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span> \
            Adresa nije ni na jednoj od lista planiranih isključenja. \
            </div>"),
        "match" : _.template("<div class=\"alert alert-warning alert-dismissible\" role=\"alert\"> \
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Zatvori\"><span aria-hidden=\"true\">&times;</span></button> \
            <span class=\"glyphicon glyphicon-warning-sign\" aria-hidden=\"true\"></span> \
            Adresa je na listi planiranih isključenja: \
            <ul><% _.each(result, function(item) { %> <li>dan: <%= _.template.dateformat(item.day) %>, period: <%= _.template.hourformat(item.period.start) %> - <%= _.template.hourformat(item.period.end) %>, više informacija: <a href=\"<%= item.more_info %>\" target=\"_blank\"><%= item.more_info %></a></li> <% }); %><ul> \
            </div>"),
        "subscribed" : _.template("<div class=\"alert alert-success alert-dismissible\" role=\"alert\"> \
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Zatvori\"><span aria-hidden=\"true\">&times;</span></button> \
            <span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span> \
            Uspešno ste izvršili prijavu. Poruka za potvrdu je poslata na vašu email adresu. \
            </div>"),
        "error" : _.template("<div class=\"alert alert-danger alert-dismissible\" role=\"alert\"> \
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Zatvori\"><span aria-hidden=\"true\">&times;</span></button> \
            <span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> \
            <%= error %> \
            </div>"),
        "errorDuplicateSubscription" : _.template("<div class=\"alert alert-danger alert-dismissible\" role=\"alert\"> \
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Zatvori\"><span aria-hidden=\"true\">&times;</span></button> \
            <span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> \
            Identična prijava već postoji. \
            </div>")
    });

    function initStreetAutocomplete() {
        $.get(STREET_NAMES_REPO, function(data){
            $("input[name=address\\[street\\]]", $form).typeahead({ source:data });
        }, "json");
    }

    function initFormActions() {
        $("#subform-subscribe").on("show.bs.collapse", function () {
            msgRenderer.clear();
        });

        $("button[type=submit]", $form).click(function() {
            $formButton = $(this);
            action = $formButton.attr("name");
        });

        $form.submit(function(e) {
            var isCheck = (action === "check");
            var isSubscribe = !isCheck;

            msgRenderer.clear();

            if ($.trim($("input[name=address\\[street\\]]", $form).val()) === "") {
                msgRenderer.renderTemplate("error", {error: $("input[name=address\\[street\\]]", $form).data("error-message")}, true);
                return false;
            } else if ($.trim($("input[name=address\\[number\\]]", $form).val()) === "") {
                msgRenderer.renderTemplate("error", {error: $("input[name=address\\[number\\]]", $form).data("error-message")}, true);
                return false;
            } else if (isSubscribe) {
                var subscribeEmail = $.trim($("input[name=contact\\[value\\]]", $form).val());
                if (subscribeEmail === "") {
                    msgRenderer.renderTemplate("error", {error: $("input[name=contact\\[value\\]]", $form).data("error-message")}, true);
                    return false;
                } else if (!/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(subscribeEmail)) {
                    msgRenderer.renderTemplate("error", {error: $("input[name=contact\\[value\\]]", $form).data("error-message-email")}, true);
                    return false;
                }
            }

            $formButton.prop("disabled", true);

            $.ajax({
                url: actions[action],
                type: isCheck ? "get" : "post",
                data: $form.serialize(),
                dataType: "json"
            }).done(function(response) {
                if (isCheck) {
                    if (response.result.length === 0) {
                        msgRenderer.renderTemplate("noMatches", {}, true);
                    } else {
                        msgRenderer.renderTemplate("match", response, false);
                    }
                } else if (isSubscribe) {
                    msgRenderer.renderTemplate("subscribed", {}, true);
                }
            }).fail(function(response) {
                var error = response.responseJSON;
                if (error.code === 3100) {
                    msgRenderer.renderTemplate("errorDuplicateSubscription", {});
                    return;
                }
                msgRenderer.renderTemplate("error", error);
            }).always(function() {
                $formButton.prop("disabled", false);
            });

            return false;
        });
    }

    return {
        init: function(){
            initStreetAutocomplete();
            initFormActions();
        }
    };
})(jQuery, _, MessageRenderer);

var contactModule = (function ($, MessageRenderer) {
    function initForm() {
        var $form = $("#contact form");
        var $formButton = $("button[type=submit]", $form);

        var msgRenderer = new MessageRenderer("#contact [role=result]");

        $form.submit(function(e) {
            $formButton.prop("disabled", true);

            $.ajax({
                url: $form.attr("action"),
                type: $form.attr("method"),
                data: $form.serialize(),
                dataType: "json"
            }).done(function(response) {
                msgRenderer.render("<div class=\"alert alert-success\" role=\"alert\">Poruka je uspešno poslata</div>", true);
            }).fail(function(response) {
                msgRenderer.render("<div class=\"alert alert-danger\" role=\"alert\">" + response.responseJSON + "</div>", true);
            }).always(function() {
                $form[0].reset();
                $formButton.prop("disabled", false);
            });

            return false;
        });
    }

    return {
        init: function(){
            initForm();
        }
    };
})(jQuery, MessageRenderer);

checkModule.init();
contactModule.init();
