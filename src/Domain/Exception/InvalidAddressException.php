<?php

namespace Dnbk\Domain\Exception;

use Dnbk\Exception\ClientErrorInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class InvalidAddressException extends InvalidArgumentException implements
    ClientErrorInterface
{
    public static function missingStreet()
    {
        return new self('Address street name must be supplied', ExceptionCodes::CODE_ADDRESS_MISSING_STREET);
    }

    public static function missingNumber()
    {
        return new self('Address number must be supplied', ExceptionCodes::CODE_ADDRESS_MISSING_NUMBER);
    }
}
