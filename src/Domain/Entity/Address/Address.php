<?php

namespace Dnbk\Domain\Entity\Address;

use Dnbk\Domain\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class Address
{
    /**
     * @var Street
     */
    private $street;

    /**
     * @var string
     */
    private $streetNumber;

    private function __construct()
    {
    }

    public static function create(Street $street, $number)
    {
        $number = trim((string) $number);

        if ($number === '') {
            throw Exception\InvalidAddressException::missingNumber();
        }

        $address = new self();
        $address->street = $street;
        $address->streetNumber = $number;

        return $address;
    }

    /**
     * @param array $data
     * @return self
     */
    public static function createFromArray(array $data)
    {
        if (!isset($data['street'])) {
            throw Exception\InvalidAddressException::missingStreet();
        }

        if (!isset($data['number'])) {
            throw Exception\InvalidAddressException::missingNumber();
        }

        $street = (is_array($data['street']))
            ? Street::createFromArray($data['street'])
            : Street::createFromString($data['street']);

        return self::create($street, $data['number']);
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getStreetNumber()
    {
        return $this->streetNumber;
    }

    public function equals(Address $address)
    {
        return $address->getStreet()->equals($this->getStreet())
            && $address->getStreetNumber() === $this->getStreetNumber();
    }

    public function isInRange($firstNumber, $lastNumber)
    {
        $streetNumber = (int) $this->streetNumber;

        return (
            $streetNumber >= $firstNumber && $streetNumber <= $lastNumber
            && ($firstNumber + $streetNumber) % 2 === 0 //event/odd street numbers
        );
    }

    public function __toString()
    {
        $address = $this->getStreet()->getName() . ' ' . $this->getStreetNumber();

        if (null !== ($municipality = $this->getStreet()->getMunicipality())) {
            $address .= ' (' . $municipality->getName() . ')';
        }

        return $address;
    }

    public function toShortString()
    {
        return $this->getStreet()->getName() . ' ' . $this->getStreetNumber();
    }
}
