<?php

namespace Dnbk\Tests\Entity;

use PHPUnit_Framework_TestCase as TestCase;
use Dnbk\Domain\Entity\Subscription;
use Dnbk\Domain\Entity\Address\Address;
use Dnbk\Domain\Entity\Address\Street;
use Dnbk\Domain\Entity\Contact;
use Dnbk\Domain\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class SubscriptionTest extends TestCase
{
    public function testCreationFailsIfAddressTypeIsNotValid()
    {
        $this->setExpectedException(
            Exception\InvalidArgumentException::class,
            'should be either array or ' . Address::class
        );

        Subscription::createNew('invalid', []);
    }

    public function testCreationFailsIfAddressStreetIsMissing()
    {
        $this->setExpectedException(Exception\InvalidSubscriptionException::class,
            'street name must be supplied'
        );

        Subscription::createNew(['number' => 1], []);
    }

    public function testCreationFailsIfAddressNumberIsMissing()
    {
        $this->setExpectedException(
            Exception\InvalidSubscriptionException::class,
            'number must be supplied'
        );

        Subscription::createNew(
            ['street' => ['name' => 'Test']],
            []
        );
    }

    public function testCreationFailsIfContactTypeIsNotValid()
    {
        $this->setExpectedException(
            Exception\InvalidArgumentException::class,
            'should be either array or ' . Contact::class
        );

        Subscription::createNew(
            ['street' => ['name' => 'Test'], 'number' => 1],
            'invalid'
        );
    }

    public function testInvalidContactExceptionIsWrapped()
    {
        $this->setExpectedException(
            Exception\InvalidSubscriptionException::class,
            "'unsupported' is not supported"
        );

        Subscription::createNew(
            ['street' => ['name' => 'Test'], 'number' => 1],
            ['type' => 'unsupported', 'value' => 1]
        );
    }

    public function testSubscriptionProperlyCreated()
    {
        $subscription = Subscription::createNew(
            ['street' => ['name' => 'Test'], 'number' => 1],
            ['type' => Contact::TYPE_EMAIL, 'value' => 'test@example.com']
        );

        $this->assertInstanceOf(Subscription::class, $subscription);
    }

    public function testCreationAcceptsAddressInstance()
    {
        $subscription = Subscription::createNew(
            Address::create(Street::create('Test'), 1),
            ['type' => Contact::TYPE_EMAIL, 'value' => 'test@example.com']
        );

        $this->assertInstanceOf(Subscription::class, $subscription);
    }

    public function testCreationAcceptsContactInstance()
    {
        $subscription = Subscription::createNew(
            Address::create(Street::create('Test'), 1),
            Contact::createNew(['type' => Contact::TYPE_EMAIL, 'value' => 'test@example.com'])
        );

        $this->assertInstanceOf(Subscription::class, $subscription);
    }

    public function testCreationProperlySetsAddress()
    {
        $subscription = Subscription::createNew(
            Address::create(Street::create('Test'), 1),
            ['type' => Contact::TYPE_EMAIL, 'value' => 'test@example.com']
        );

        $this->assertEquals('Test', $subscription->getAddress()->getStreet()->getName());
        $this->assertEquals(1, $subscription->getAddress()->getStreetNumber());
    }

    public function testCreationProperlySetsContact()
    {
        $subscription = Subscription::createNew(
            Address::create(Street::create('Test'), 1),
            ['type' => Contact::TYPE_EMAIL, 'value' => 'test@example.com']
        );

        $this->assertTrue($subscription->getContact()->isEmail());
        $this->assertEquals('test@example.com', $subscription->getContact()->getValue());
    }

    public function testContactSetterMethod()
    {
        $subscription = Subscription::createNew(
            Address::create(Street::create('Test'), 1),
            ['type' => Contact::TYPE_EMAIL, 'value' => 'test@example.com']
        );

        $newContact = Contact::createNew(['type' => Contact::TYPE_MOBILE, 'value' => '123456']);
        $subscription->setContact($newContact);

        $this->assertTrue($subscription->getContact()->isMobile());
        $this->assertEquals('123456', $subscription->getContact()->getValue());
    }

    public function testCreationProperlySetsDefaults()
    {
        $subscription = Subscription::createNew(
            Address::create(Street::create('Test'), 1),
            ['type' => Contact::TYPE_EMAIL, 'value' => 'test@example.com']
        );

        $this->assertFalse($subscription->isActive());
        $this->assertTrue(\Ramsey\Uuid\Uuid::isValid($subscription->getUid()));
    }

    public function testConfirmationMissmatch()
    {
        $subscription = Subscription::createNew(
            Address::create(Street::create('Test'), 1),
            ['type' => Contact::TYPE_EMAIL, 'value' => 'test@example.com']
        );

        $this->setExpectedException(
            Exception\InvalidSubscriptionCodeException::class,
            'Subscription code mismatch'
        );

        $subscription->confirm('no match');
    }

    public function testSuccessfullConfirmation()
    {
        $subscription = Subscription::createNew(
            Address::create(Street::create('Test'), 1),
            ['type' => Contact::TYPE_EMAIL, 'value' => 'test@example.com']
        );

        $this->assertFalse($subscription->isActive());

        $subscription->confirm($subscription->getUid());

        $this->assertTrue($subscription->isActive());
    }
}
