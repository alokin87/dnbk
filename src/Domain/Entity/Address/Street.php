<?php

namespace Dnbk\Domain\Entity\Address;

use Dnbk\Domain\Exception;
use Dnbk\Util\Stringy;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class Street
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Municipality
     */
    private $municipality = null;

    /**
     * @var string
     */
    private $normalizedName = null;

    private function __construct()
    {
    }

    public static function create($name, $municipality = null)
    {
        if ($name instanceof self) {
            return $name;
        }

        $name = trim($name);

        if ($name === '') {
            throw Exception\InvalidStreetException::missingName();
        }

        $street = new self();

        $street->name = trim($name);

        if (null !== $municipality) {
            if (!$municipality instanceof Municipality) {
                $municipality = Municipality::create($municipality);
            }
            $street->municipality = $municipality;
        }

        return $street;
    }

    /**
     * @param array $data
     * @return self
     */
    public static function createFromArray(array $data)
    {
        if (!isset($data['name'])) {
            throw Exception\InvalidStreetException::missingName();
        }

        if (isset($data['municipality'])) {
            return self::create($data['name'], $data['municipality']);
        }

        return self::createFromString($data['name']);
    }

    /**
     * @param string $streetString
     * @return self
     */
    public static function createFromString($streetString)
    {
        $streetString = trim($streetString);

        if (substr($streetString, -1) === ')' && false !== ($municipalitySep = strpos($streetString, '('))) {
            $streetName = trim(substr($streetString, 0, $municipalitySep));
            $municipality = trim(substr($streetString, $municipalitySep + 1, -1));
        } else {
            $streetName = $streetString;
            $municipality = null;
        }

        return self::create($streetName, $municipality);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getMunicipality()
    {
        return $this->municipality;
    }

    public function getNormalizedName()
    {
        if (null === $this->normalizedName) {
            $normalizedName = (string) Stringy::create($this->getName())->toAscii();
            $normalizedName = strtolower($normalizedName);
            $normalizedName = str_replace('-', ' ', $normalizedName);
            $normalizedName = preg_replace('/[^a-z0-9\s]/', '', $normalizedName);

            $this->normalizedName = $normalizedName;
        }

        return $this->normalizedName;
    }

    public function equals(Street $street)
    {
        $municipality1 = $this->getMunicipality();
        $municipality2 = $street->getMunicipality();

        if (
            (null === $municipality1 && null !== $municipality2)
            || (null !== $municipality1 && null === $municipality2)
            || (null !== $municipality1 && null !== $municipality2 && !$municipality1->equals($municipality2))
        ) {
            return false;
        }

        return $street->getName() === $this->getName();
    }

    public function like(Street $street, $threshold = 90)
    {
        if (
            null === ($municipality = $street->getMunicipality())
            || $municipality->equals($this->getMunicipality())
        ) {
            $name1 = $this->getNormalizedName();
            $name2 = $street->getNormalizedName();

            if (
                $name1 === $name2
                || (
                    strpos($name2, $name1) === 0
                    && ($length1 = strlen($name1)) >= 12 && ($length2 = strlen($name2)) >= 12
                    && ($length2 - $length1) <= 5)
            ) {
                return true;
            } else {
                $percentage = 0;
                similar_text($name1, $name2, $percentage);

                if ($percentage >= $threshold) {
                    return true;
                }
            }
        }

        return false;
    }

    public function __toString()
    {
        $street = $this->getName();

        if (null !== ($municipality = $this->getMunicipality())) {
            $street .= ' (' . $municipality->getName() . ')';
        }

        return $street;
    }
}
