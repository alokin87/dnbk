<?php

return [
    'db' => [
        'driver'    => 'mysql',
        'host'      => 'localhost',
        'database'  => '',
        'username'  => '',
        'password'  => '',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
    ],

    'dependencies' => [
        'factories' => [
            Illuminate\Database\Capsule\Manager::class => function($c) {
                $capsule = new Illuminate\Database\Capsule\Manager();

                $capsule->addConnection($c['config']['db']);
                $capsule->setAsGlobal();
                $capsule->bootEloquent();

                return $capsule;
            },
        ],
    ],
];
