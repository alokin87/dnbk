<?php

namespace Dnbk\Domain\Repository;

use Dnbk\Domain\Entity\Contact;
use Dnbk\Domain\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class ContactRepository implements ContactRepositoryInterface
{
    /**
     * @var Contact
     */
    private $model;

    public function __construct(Contact $contactModel = null)
    {
        $this->model = $contactModel ?: new Contact();
    }

    public function findByType($type)
    {
        return $this->model->where('type', $type)->get();
    }

    public function findOne($value, $type = null)
    {
        if ($value instanceof Contact) {
            $type = $value->getType();
            $value = $value->getValue();
        } elseif (null === $type) {
            throw new Exception\InvalidArgumentException("'type' argument is missing");
        }

        return $this->model->where([
            'type' => $type,
            'value' => $value
        ])->first();
    }

    public function save(Contact $contact)
    {
        $contact->save();
    }
}
