<?php

namespace Dnbk\Sms;

use Notify\Message\MessageInterface;
use Notify\Message\Actor\Recipients;
use Notify\Message\Actor\Actor;
use Notify\Contact\GenericContact;
use Notify\Message\Content\TextContent;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class Message implements MessageInterface
{
    /**
     * @var string
     */
    private $to;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $from;

    public function __construct($to, $message, $from = null)
    {
        $this->to = $to;
        $this->message = $message;
        $this->from = $from;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function getRecipients()
    {
        return new Recipients([
            new Actor(new GenericContact($this->to))
        ]);
    }

    public function getContent()
    {
        return new TextContent($this->getMessage());
    }
}
