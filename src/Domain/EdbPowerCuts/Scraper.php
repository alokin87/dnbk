<?php

namespace Dnbk\Domain\EdbPowerCuts;

use DOMDocument;
use DateTime;
use DateTimeZone;
use DateTimeImmutable;
use Dnbk\Util\Stringy;
use Dnbk\Domain\Entity\Address\Street;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class Scraper implements
    ScraperInterface,
    LoggerAwareInterface
{
    const BASE_URL = 'http://www.epsdistribucija.rs';

    /**
     * @var callable
     */
    private $fetcher;

    private static $altStreetNames = [
        'BUL KRALJ.ALEKSANDRA'=>['BULEVAR KRALJA ALEKSANDRA'],
        'DVADESETSEDMOG MARTA' => ['27. MARTA'],
        'DVADESETOG OKTOBRA' => ['20. OKTOBRA'],
        '29 NOVEMBRA' => ['29. NOVEMBRA', 'DVADESETDEVETOG NOVEMBRA', 'BULEVAR DESPOTA STEFANA'],
        'BUL. DESPOTA STEFANA' => ['29. NOVEMBRA', 'DVADESETDEVETOG NOVEMBRA', 'BULEVAR DESPOTA STEFANA'],
        'BUL.KN.A.KARAĐORĐEVIĆA' => ['BULEVAR KNEZA ALEKSANDRA KARAĐORĐEVIĆA'],
        'KNEZA MIHAILA' => ['KNEZ MIHAILOVA'],
        'BULEVAR MIHAJLA PUPINA' => ['BULEVAR MIHAILA PUPINA'],
    ];

    /**
     * @var DateTimeZone
     */
    private $scheduleTimezone;

    /**
     * @var DateTimeZone
     */
    private $systemTimezone;

    use LoggerAwareTrait;

    public function __construct($fetcher = null, LoggerInterface $logger = null)
    {
        if (!$fetcher) {
            $fetcher = function($url) {
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $response = curl_exec($ch);

                curl_close($ch);

                return $response;
            };
        } elseif (!is_callable($fetcher)) {
            throw new Exception\InvalidArgumentException('Supplied fetcher is not callable');
        }

        $this->fetcher = $fetcher;

        if (null !== $logger) {
            $this->setLogger($logger);
        }

        $this->scheduleTimezone = new DateTimeZone('Europe/Belgrade');
        $this->systemTimezone = new DateTimeZone(date_default_timezone_get());
    }

    private function getScheduleUrl($day)
    {
        return self::BASE_URL . '/' . sprintf('Dan_%s_Iskljucenja.htm', $day);
    }

    private function fetchWebPage($day)
	{
        $url = $this->getScheduleUrl($day);

        $fetcher = $this->fetcher;

        $response = null;
        try {
            $response = $fetcher($url);
        } catch (\Exception $ex) {
            if (null !== $this->logger) {
                $this->logger->error('Page cannot be fetched', [
                    'day' => $day,
                    'error' => $ex,
                ]);
            }

            throw Exception\ScraperException::pageCantBeFetched($day);
        }

        if (false === $response) {
            if (null !== $this->logger) {
                $this->logger->error('EDB power cuts page cannot be fetched');
            }

            throw Exception\ScraperException::pageCantBeFetched($day);
        }

		return $response;
	}

    /**
     * @param DateTime $date
     * @return Schedule
     */
    public function getSchedule(DateTime $date)
    {
        $date = DateTimeImmutable::createFromFormat(DateTime::ISO8601, $date->format(DateTime::ISO8601), $date->getTimezone());
        $date = $date->setTime(0, 0, 0);

        $now = new DateTime('now', $this->scheduleTimezone);
        $now->setTime(0, 0, 0);

        $scheduleDate = $date->setTimezone($this->scheduleTimezone);

        $interval = $now->diff($scheduleDate);
        $dayNum = $interval->days;

        $list = [];

        $html = $this->fetchWebPage($dayNum);

        if (stripos('<head>', $html) === false) {
            $html = str_ireplace('<html>', '<html><head><meta http-equiv="content-type" content="text/html; charset=utf-8" /></head>', $html);
        }

        $dom = new DOMDocument();
        @ $dom->loadHTML($html);

        $mainTable = $dom->getElementsByTagName('table')->item(1);

        if (!$mainTable) {
            return Schedule::createEmpty($date);
        }

        $dateString = $scheduleDate->format('Y-m-d');

        foreach ($mainTable->getElementsByTagName('tr') as $key => $row) {
            if ($key == 0) {
                continue;
            }

            $municipality = (string) Stringy::create($row->getElementsByTagName('td')->item(0)->textContent)->trim()->toLatin();

            $powerCutPeriod = trim($row->getElementsByTagName('td')->item(1)->textContent);
            $powerCutPeriod = explode('-', $powerCutPeriod);
            $powerCutStartDate = new DateTimeImmutable($dateString . ' ' . trim($powerCutPeriod[0]), $this->scheduleTimezone);
            $powerCutEndDate = new DateTimeImmutable($dateString . ' ' . trim($powerCutPeriod[1]), $this->scheduleTimezone);
            $powerCutPeriod[0] = $powerCutStartDate->setTimezone($this->systemTimezone);
            $powerCutPeriod[1] = $powerCutEndDate->setTimezone($this->systemTimezone);

            $addresses = (string) Stringy::create($row->getElementsByTagName('td')->item(2)->textContent)->trim()->toLatin();
            $addresses = $this->filterAddresses($addresses);

            $addressesList = explode(', ', $addresses); //Naziv ulice, brojevi.

            foreach ($addressesList as $data) {
                $streetNameRanges = explode(': ', $data);

                $streetName = trim($streetNameRanges[0]);
                $streetNumberRanges = (isset($streetNameRanges[1])) ? $streetNameRanges[1] : [];

                $list[] = ScheduleItem::create(
                    Street::create($streetName, $municipality),
                    $streetNumberRanges,
                    $powerCutPeriod
                );

                if (isset(self::$altStreetNames[$streetName])) {
                    foreach (self::$altStreetNames[$streetName] as $altStreetName) {
                        $list[] = ScheduleItem::create(
                            Street::create($altStreetName, $municipality),
                            $streetNumberRanges,
                            $powerCutPeriod
                        );
                    }
                }
            }
        }

        return Schedule::create($date, $list, $this->getScheduleUrl($dayNum));
    }

    private function filterAddresses($addressesInfo)
    {
        if (strpos($addressesInfo, 'Naselje') !== false) {
            $addressesInfo = preg_replace('/Naselje:?\s[a-zA-ZČĆŠĐŽčćšđž\s\.]+:/', '', $addressesInfo); //Brisanje dela stringa koji sadrzi "Naselje: ...".
        }

        if (strpos($addressesInfo, '(') !== false) {
            $addressesInfo = preg_replace('/\(.+\)/', '', $addressesInfo); //Brisanje nepotrebnog dela u zagradama.
        }

        $addressesInfo = rtrim($addressesInfo, ','); //Uklanjanje suvisnog zareza na kraju liste ulica i brojeva.

        return $addressesInfo;
    }
}
