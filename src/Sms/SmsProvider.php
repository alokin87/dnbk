<?php

namespace Dnbk\Sms;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
interface SmsProvider
{
    public function send(Message $message);
}
