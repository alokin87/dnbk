<?php

namespace Dnbk\Container;

use Interop\Container\ContainerInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
interface ContainerAwareInterface
{
    public function setContainer(ContainerInterface $container);

    public function getContainer();
}
