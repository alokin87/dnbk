<?php

namespace Dnbk\Notification\Message\SendService;

use Notify\Message\SendService\SendServiceInterface;
use Notify\Message\MessageInterface;
use Notify\Message\EmailMessage;
use Nette\Mail\IMailer;
use Nette\Mail\Message as NetteEmailMessage;
use Notify\Message\SendService\Exception\UnsupportedMessageException;

/**
 *
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class EmailSendService implements SendServiceInterface
{
    /**
     * @var IMailer
     */
    private $mailer;

    public function __construct(IMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * {@inheritDoc}
     */
    public function send(MessageInterface $message)
    {
        if (! $message instanceof EmailMessage) {
            throw UnsupportedMessageException::fromSendServiceAndMessage($this, $message);
        }

        /* @var $message EmailMessage */

        $emailMessage = new NetteEmailMessage();

        foreach ($message->getRecipients() as $recipient) {
            $emailMessage->addTo($recipient->getContact()->getValue(), $recipient->getName());
        }

        $emailMessage->setSubject($message->getSubject());

        if ($message->getOptions()->get('html', true)) {
            $emailMessage->setHtmlBody($message->getContent());
        } else {
            $emailMessage->setBody($message->getContent());
        }

        $this->mailer->send($emailMessage);
    }
}
