<?php

namespace Dnbk\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Dnbk\Domain\Service\CheckerServiceInterface;
use DatePeriod;
use DateTime;
use DateInterval;
use Dnbk\Container\ContainerAwareInterface;
use Dnbk\Container\ContainerAwareTrait;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class SubscriptionsCheckerCommand extends Command implements
    ContainerAwareInterface,
    LoggerAwareInterface
{
    use ContainerAwareTrait;
    use LoggerAwareTrait;

    /**
     * @var CheckerServiceInterface
     */
    private $checkerService;

    /**
     * @var float
     */
    private $elapsedTime;

    protected function configure()
    {
        $this
            ->setName('cron:subscriptions_checker')
            ->setDescription('Periodically checks whether subscriptions are scheduled and sends notifications.')
        ;
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->checkerService = $this->getContainer()->get(CheckerServiceInterface::class);
        $this->setLogger($this->getContainer()->get('SubscriptionsCheckerLogger'));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);

        $start = microtime(true);

        $period = new DatePeriod(new DateTime(), new DateInterval('P1D'), 1, DatePeriod::EXCLUDE_START_DATE); //tomorrow
        $matches = $this->checkerService->checkSubscriptions($period, true);

        $end = microtime(true);

        $this->elapsedTime = $elapsedTime = round($end - $start, 2);

        $output->writeln("Matches count: " . count($matches));
        $output->writeln("Elapsed time: $elapsedTime");

        $this->log($matches);
    }

    private function log(array $matches)
    {
        if (0 === count($matches)) {
            return;
        }
        
        $subscriptions = [];

        foreach ($matches as $match) {
            $subscriptions[] = $match->getSubscription();
        }

        $this->logger->info('Auto-check completed', [
            'subscriptions' => json_encode($subscriptions, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE),
            'time' => $this->elapsedTime
        ]);
    }
}
