<?php

namespace Dnbk\Domain\Repository;

use Dnbk\Domain\Entity\Contact;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
interface ContactRepositoryInterface
{
    /**
     * @param int $type
     * @return Contact[]
     */
    public function findByType($type);

    /**
     * @param Contact|string $value Contact instance or value string
     * @param int $type OPTIONAL
     * @return Contact|null
     */
    public function findOne($value, $type = null);

    /**
     * @param Contact $contact
     * @return void
     */
    public function save(Contact $contact);
}
