<?php

namespace Dnbk\Domain\Exception;

use Dnbk\Exception\ClientErrorInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class InvalidStreetException extends InvalidArgumentException implements
    ClientErrorInterface
{
    public static function missingName()
    {
        return new self('Street name must be supplied', ExceptionCodes::CODE_STREET_MISSING_NAME);
    }
}
