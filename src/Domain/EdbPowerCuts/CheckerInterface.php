<?php

namespace Dnbk\Domain\EdbPowerCuts;

use DateTime;
use DatePeriod;
use Dnbk\Domain\Entity\Address\Address;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
interface CheckerInterface
{
    /**
     * Checks whether some Address is scheduled for a specific date.
     *
     * @param Address $address
     * @param DateTime $date
     * @return ScheduleItem[] Schedule items collection (if any).
     */
    public function checkForDate(Address $address, DateTime $date);

    /**
     * Checks whether Address is scheduled in some period.
     *
     * @param Address $address
     * @param DatePeriod $period
     * @return ScheduleItem[] Schedule items collection (if any).
     */
    public function checkForPeriod(Address $address, DatePeriod $period);
}
