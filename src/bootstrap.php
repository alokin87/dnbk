<?php

chdir(dirname(__DIR__));

require 'vendor/autoload.php';

/** @var \Interop\Container\ContainerInterface $container */
$container = Dnbk\Container\ContainerFactory::create(require 'config/config.php');

$config = ($container->has('config')) ? $container->get('config') : [];

//Set PHP settings
if (!isset($config['php_settings'])) {
    foreach ($config['php_settings'] as $name => $value) {
        ini_set($name, $value);
    }
}

date_default_timezone_set('UTC');

//Boot Eloquent
if ($container->has(Illuminate\Database\Capsule\Manager::class)) {
    $container->get(Illuminate\Database\Capsule\Manager::class);
}

Dnbk\Notification\Message\Content\TemplatedContentProvider::setDefaultTemplateRenderer(
    $container->get(Zend\Expressive\Template\TemplateRendererInterface::class)
);

Notify\AbstractNotification::setDefaultStrategy($container->get('Notify\Strategy\SendStrategy'));

return $container;
