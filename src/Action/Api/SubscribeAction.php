<?php

namespace Dnbk\Action\Api;

use Dnbk\Domain\Service\SubscriptionServiceInterface as SubscriptionService;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\JsonResponse;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class SubscribeAction
{
    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    public function __construct(SubscriptionService $subscriptionService)
    {
        $this->subscriptionService = $subscriptionService;
    }

    /**
     * @SWG\Post(
     *  path="/subscriptions",
     *  @SWG\Parameter(
     *      name="subscription",
     *      description="Subscription to be added",
     *      required=true,
     *      in="body",
     *      @SWG\Schema(ref="#/definitions/NewSubscription")
     *  ),
     *  @SWG\Response(
     *      response=200,
     *      description="Subscription added"
     *  ),
     *  @SWG\Response(
     *      response=400,
     *      description="Invalid input"
     *  ),
     *  @SWG\Response(
     *      response="default",
     *      description="Error response",
     *      @SWG\Schema(
     *          ref="#/definitions/Error"
     *      )
     *  )
     * )
     *
     * @SWG\Definition(
     *  definition="NewSubscription",
     *  required={"address", "contact"},
     *  @SWG\Property(
     *      property="address",
     *      required={"street", "number"},
     *      properties={
     *          @SWG\Property(
     *              property="street",
     *              type="string"
     *          ),
     *          @SWG\Property(
     *              property="number",
     *              type="string"
     *          )
     *      }
     *  ),
     *  @SWG\Property(
     *      property="contact",
     *      required={"type", "value"},
     *      properties={
     *          @SWG\Property(
     *              property="type",
     *              type="string",
     *              enum={"email", "mobile"}
     *          ),
     *          @SWG\Property(
     *              property="value",
     *              type="string"
     *          )
     *      }
     *  )
     * )
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $params = $request->getParsedBody();

        $subscription = $this->subscriptionService->subscribe($params);

        return new JsonResponse($subscription);
    }
}
