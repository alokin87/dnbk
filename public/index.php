<?php

/**
 * @SWG\Swagger(
 *     basePath="/api",
 *     host="danebudekasno.nikolaposa.in.rs",
 *     schemes={"http"},
 *     produces={
 *      "application/json"
 *     },
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="DNBK API",
 *         @SWG\Contact(name="Nikola Poša", url="http://www.nikolaposa.in.rs")
 *     ),
 *     @SWG\Definition(
 *         definition="Error",
 *         required={"error", "code"},
 *         @SWG\Property(
 *             property="error",
 *             type="string"
 *         ),
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         )
 *     )
 * )
 */

/** @var \Interop\Container\ContainerInterface $container */
$container = require __DIR__ . '/../src/bootstrap.php';

/** @var \Zend\Expressive\Application $app */
$app = $container->get('Zend\Expressive\Application');
$app->run();
