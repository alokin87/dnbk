<?php

namespace Dnbk\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Nette\Mail\IMailer as Mailer;
use Nette\Mail\Message as MailMessage;
use Dnbk\Exception\RuntimeException;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class ContactAction
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var array
     */
    private $options;

    public function __construct(Mailer $mailer, array $options)
    {
        $this->mailer = $mailer;
        $this->options = $options;
    }

    private function getParams(ServerRequestInterface $request)
    {
        $params = $request->getParsedBody();

        if (!isset($params['name'])) {
            throw Exception\MissingParamException::param('name');
        }

        $name = trim($params['name']);

        if ('' === $name) {
            throw Exception\InvalidParamException::required('name');
        }

        if (!isset($params['email'])) {
            throw Exception\MissingParamException::param('email');
        }

        $email = trim($params['email']);

        if ('' === $email) {
            throw Exception\InvalidParamException::required('email');
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw Exception\InvalidParamException::invalidType('email', 'email');
        }

        if (!isset($params['message'])) {
            throw Exception\MissingParamException::param('message');
        }

        $message = trim($params['message']);

        if ('' === $message) {
            throw Exception\InvalidParamException::required('message');
        }

        return [
            'name' => $name,
            'email' => $email,
            'message' => $message,
        ];
    }

    private function send(array $params)
    {
        $message = new MailMessage();
        $message->addTo($this->options['to']);
        $message->setSubject($this->options['subject']);
        $message->setBody($params['message']);
        $message->setFrom($params['email'], $params['name']);
        $message->setHeader('Reply-To', $params['email']);

        try {
            $this->mailer->send($message);
        } catch (\Nette\Mail\SendException $ex) {
            throw new RuntimeException('Došlo je do greške pri pokušaju slanja vaše poruke. Pokušajte ponovo.', 500, $ex);
        }
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $params = $this->getParams($request);
        $this->send($params);

        return new JsonResponse(['success' => true]);
    }

}
