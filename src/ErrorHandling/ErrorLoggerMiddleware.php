<?php

namespace Dnbk\ErrorHandling;

use Zend\Stratigility\ErrorMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Dnbk\Exception\ClientErrorInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class ErrorLoggerMiddleware implements ErrorMiddlewareInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    public function __invoke($error, ServerRequestInterface $request, ResponseInterface $response, callable $out = null)
    {
        if ($this->shouldLog($error)) {
            $this->logger->error($error);
        }

        return $out($request, $response, $error);
    }

    private function shouldLog($error)
    {
        return !$error instanceof ClientErrorInterface;
    }
}
