<?php

namespace Dnbk\Template;

use Interop\Container\ContainerInterface;
use Zend\Expressive\Plates\PlatesRenderer;
use League\Plates\Engine as PlatesEngine;
use League\Plates\Extension\Asset as AssetPlatesExt;
use Dnbk\Template\Extension\UrlHelperExtension;
use Dnbk\Helper\UrlHelper;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class PlatesRendererFactory
{
    /**
     * @param ContainerInterface $container
     * @return PlatesRenderer
     */
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->has('config') ? $container->get('config') : [];
        $config = isset($config['templates']) ? $config['templates'] : [];

        // Create the engine instance:
        $engine = new PlatesEngine();

        // Set file extension
        if (isset($config['extension'])) {
            $engine->setFileExtension($config['extension']);
        }

        $engine->loadExtension(new UrlHelperExtension($container->get(UrlHelper::class)));

        if (isset($config['assets'])) {
            $engine->loadExtension(new AssetPlatesExt($config['assets'], true));
        }

        // Inject engine
        $plates = new PlatesRenderer($engine);

        // Add template paths
        $allPaths = isset($config['paths']) && is_array($config['paths']) ? $config['paths'] : [];
        foreach ($allPaths as $namespace => $paths) {
            $namespace = is_numeric($namespace) ? null : $namespace;
            foreach ((array) $paths as $path) {
                $plates->addPath($path, $namespace);
            }
        }

        return $plates;
    }
}
