<?php

namespace Dnbk\Action;

use Dnbk\Domain\Service\SubscriptionServiceInterface as SubscriptionService;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class ConfirmSubscriptionAction
{
    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var TemplateRendererInterface
     */
    private $templateRenderer;

    public function __construct(SubscriptionService $subscriptionService, TemplateRendererInterface $templateRenderer)
    {
        $this->subscriptionService = $subscriptionService;
        $this->templateRenderer = $templateRenderer;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $params = $request->getQueryParams();

        if (!isset($params['c'])) {
            throw Exception\MissingParamException::param('code');
        }

        $this->subscriptionService->confirm($params['c']);

        return new HtmlResponse($this->templateRenderer->render('app::subscription_confirmed'));
    }
}
