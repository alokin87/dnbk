<?php

namespace Dnbk\Notification\Message\SendService;

use Notify\Message\SendService\SendServiceInterface;
use Notify\Message\MessageInterface;
use Dnbk\Sms\SmsProvider;
use Dnbk\Sms\Message as SmsMessage;
use Notify\Message\SendService\Exception\UnsupportedMessageException;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class SmsSendService implements SendServiceInterface
{
    /**
     * @var SmsProvider
     */
    private $smsProvider;

    public function __construct(SmsProvider $smsProvider)
    {
        $this->smsProvider = $smsProvider;
    }

    public function send(MessageInterface $message)
    {
        if (! $message instanceof SmsMessage) {
            throw UnsupportedMessageException::fromSendServiceAndMessage($this, $message);
        }

        $this->smsProvider->send($message);
    }
}
