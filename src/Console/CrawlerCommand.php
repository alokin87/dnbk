<?php

namespace Dnbk\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Dnbk\Domain\EdbPowerCuts\ScraperInterface;
use DatePeriod;
use DateTime;
use DateInterval;
use Dnbk\Container\ContainerAwareInterface;
use Dnbk\Container\ContainerAwareTrait;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class CrawlerCommand extends Command implements
    ContainerAwareInterface,
    LoggerAwareInterface
{
    use ContainerAwareTrait;
    use LoggerAwareTrait;

    /**
     * @var ScraperInterface
     */
    private $scraper;

    protected function configure()
    {
        $this
            ->setName('cron:crawler')
            ->setDescription('Periodically crawls EDB website and scrapes power cuts schedules.')
        ;
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->scraper = $this->getContainer()->get(ScraperInterface::class);
        $this->setLogger($this->getContainer()->get('CrawlerLogger'));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $period = new DatePeriod(new DateTime(), new DateInterval('P1D'), 3);

        foreach ($period as $date) {
            $dateString = $date->format('d.m.Y');

            $output->writeln("Fetching schedule for '{$dateString}'");

            try {
                $this->scraper->getSchedule($date); //will be cached
            } catch (\Exception $ex) {
                $this->logger->error('EDB data fetching has failed', ['date' => $dateString, 'message' => $ex->getMessage()]);
                continue;
            }

            $this->logger->info('EDB data fetched', ['date' => $dateString]);
        }
    }
}
