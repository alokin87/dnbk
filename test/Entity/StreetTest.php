<?php

namespace Dnbk\Tests\Entity;

use PHPUnit_Framework_TestCase as TestCase;
use Dnbk\Domain\Entity\Address\Street;
use Dnbk\Domain\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class StreetTest extends TestCase
{
    public function testArrayCreation()
    {
        $street = Street::createFromArray([
            'name' => 'Test',
            'municipality' => 'Municipality'
        ]);

        $this->assertEquals('Test', $street->getName());
        $this->assertEquals('Municipality', $street->getMunicipality()->getName());
    }

    public function testArrayCreationFailsIfStreetIsMissing()
    {
        $this->setExpectedException(Exception\InvalidStreetException::class,
            'name must be supplied'
        );

        Street::createFromArray([]);
    }

    public function testStreetNameValidation()
    {
        $this->setExpectedException(Exception\InvalidStreetException::class,
            'name must be supplied'
        );

        Street::create('    ');
    }

    public function testStringCreation()
    {
        $street = Street::createFromString('Test (Foobar)');

        $this->assertEquals('Test', $street->getName());
        $this->assertEquals('Foobar', $street->getMunicipality()->getName());
    }

    public function testNormalizedNameRetrieval()
    {
        $street = Street::create('Sime Lozanića');

        $this->assertEquals('sime lozanica', $street->getNormalizedName());
    }

    public function testLikeComparison()
    {
        $this->assertTrue(Street::create('Knez Mihailova')->like(Street::create('Kneza Mihaila'), 85));
    }
}
