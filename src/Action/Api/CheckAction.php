<?php

namespace Dnbk\Action\Api;

use Dnbk\Domain\Service\CheckerServiceInterface as CheckerService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use DateTime;
use Dnbk\Action\Exception;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class CheckAction
{
    /**
     * @var CheckerService
     */
    private $checkerService;

    public function __construct(CheckerService $checkerService)
    {
        $this->checkerService = $checkerService;
    }

    private function getParams(ServerRequestInterface $request)
    {
        $queryParams = $request->getQueryParams();

        $address = $date = $day = null;

        if (!isset($queryParams['address'])) {
            throw Exception\MissingParamException::param('address');
        }

        $address = $queryParams['address'];

        if (!is_array($address)) {
            throw Exception\InvalidParamException::invalidType('address', 'array');
        }

        if (null !== ($day = $request->getAttribute('day'))) {
            $day = (int) $day;
        } elseif (isset($queryParams['date'])) {
            $date = trim($queryParams['date']);

            try {
                $date = new DateTime($date);
            } catch (\Exception $ex) {
                throw Exception\InvalidParamException::invalidType('date', 'date');
            }
        } elseif (isset($queryParams['day'])) {
            $day = trim($queryParams['day']);

            if (!ctype_digit($day)) {
                throw Exception\InvalidParamException::invalidType('day', 'int');
            }

            $day = (int) $day;
        }

        return [
            'address' => $address,
            'date' => $date,
            'day' => $day,
        ];
    }

    /**
     * @SWG\Get(
     *  path="/schedules/items",
     *  @SWG\Parameter(
     *      name="date",
     *      description="Date for which items should be searched",
     *      required=false,
     *      type="string",
     *      format="date",
     *      in="query"
     *  ),
     *  @SWG\Response(
     *      response="200",
     *      description="Search schedule items",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="result",
     *              type="array",
     *              @SWG\Items(ref="#/definitions/ScheduleItem")
     *          )
     *      )
     *  ),
     *  @SWG\Response(
     *      response="default",
     *      description="Error response",
     *      @SWG\Schema(
     *          ref="#/definitions/Error"
     *      )
     *  )
     * )
     *
     * @SWG\Get(
     *  path="/schedules/{day}/items",
     *  @SWG\Parameter(
     *      name="day",
     *      description="Day number - 0, 1, 2, 3",
     *      required=true,
     *      type="integer",
     *      format="int32",
     *      in="path"
     *  ),
     *  @SWG\Response(
     *      response="200",
     *      description="Search schedule items for a specific day",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="result",
     *              type="array",
     *              @SWG\Items(ref="#/definitions/ScheduleItem")
     *          )
     *      )
     *  ),
     *  @SWG\Response(
     *      response="default",
     *      description="Error response",
     *      @SWG\Schema(
     *          ref="#/definitions/Error"
     *      )
     *  )
     * )
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $params = $this->getParams($request);

        if (isset($params['date'])) {
            $scheduleItems = $this->checkerService->checkForDate($params['address'], $params['date']);
        } elseif (isset($params['day'])) {
            $scheduleItems = $this->checkerService->checkForDay($params['address'], $params['day']);
        } else {
            $scheduleItems = $this->checkerService->checkForPeriod($params['address']);
        }

        return new JsonResponse([
            'result' => $scheduleItems,
        ]);
    }
}
