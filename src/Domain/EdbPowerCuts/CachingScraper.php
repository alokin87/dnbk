<?php

namespace Dnbk\Domain\EdbPowerCuts;

use Doctrine\Common\Cache\Cache;
use DateTime;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class CachingScraper implements ScraperInterface
{
    /**
     * @var ScraperInterface;
     */
    private $scraper;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var int
     */
    private $cacheLifetime;

    public function __construct(ScraperInterface $scraper, Cache $cache, $cacheLifetime = 3600)
    {
        $this->scraper = $scraper;

        $this->cache = $cache;
        $this->cache->setNamespace('dnbk_schedule');

        $this->cacheLifetime = $cacheLifetime;
    }

    private function getCacheId(DateTime $date)
    {
        return $date->format('Y-m-d');
    }

    public function getSchedule(DateTime $date)
    {
        $cacheId = $this->getCacheId($date);

        if (false !== ($schedule = $this->cache->fetch($cacheId))) {
            return $schedule;
        }

        $schedule = $this->scraper->getSchedule($date);

        $this->cache->save($cacheId, $schedule, $this->cacheLifetime);

        return $schedule;
    }
}
