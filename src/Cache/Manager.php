<?php

namespace Dnbk\Cache;

use Dnbk\Exception;
use Doctrine\Common\Cache\Cache as CacheDriver;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
final class Manager
{
    /**
     * @var array
     */
    private $configs;

    /**
     * @var Factory
     */
    private $factory;

    public function __construct(array $configs)
    {
        $this->configs = $configs;
    }

    /**
     * @return Factory
     */
    public function getFactory()
    {
        if (null === $this->factory) {
            $this->setFactory(new Factory());
        }
        return $this->factory;
    }

    public function setFactory($factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param string $name
     * @return CacheDriver
     */
    public function get($name)
    {
        if (!isset($this->configs[$name])) {
            throw new Exception\DomainException("Unknown cache configuration: '$name'");
        }

        return $this->getFactory()->create(
            $this->configs[$name]['adapter'],
            isset($this->configs[$name]['options']) ? $this->configs[$name]['options'] : []
        );
    }

    /**
     * @param string $name
     * @return array
     */
    public function getOptions($name)
    {
        if (!isset($this->configs[$name])) {
            throw new Exception\DomainException("Unknown cache configuration: '$name'");
        }

        return isset($this->configs[$name]['options']) ? $this->configs[$name]['options'] : [];
    }
}
