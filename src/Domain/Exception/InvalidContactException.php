<?php

namespace Dnbk\Domain\Exception;

use Dnbk\Exception\ClientErrorInterface;

/**
 * @author Nikola Posa <posa.nikola@gmail.com>
 */
class InvalidContactException extends InvalidArgumentException implements
    ClientErrorInterface
{
    public static function missingType()
    {
        return new self('Contact type must be supplied', ExceptionCodes::CODE_CONTACT_MISSING_VALUE);
    }

    public static function unsupportedType($type)
    {
        return new self("Contact type '$type' is not supported", ExceptionCodes::CODE_CONTACT_UNSUPPORTED_TYPE);
    }

    public static function missingValue()
    {
        return new self('Contact value must be supplied', ExceptionCodes::CODE_CONTACT_MISSING_VALUE);
    }

    public static function invalidEmail()
    {
        return new self('Contact email address is not valid', ExceptionCodes::CODE_CONTACT_INVALID_EMAIL);
    }

    public static function typeNotResolved()
    {
        return new self("Contact type was not supplied and it cannot be resolved", ExceptionCodes::CODE_CONTACT_TYPE_NOT_RESOLVED);
    }
}
