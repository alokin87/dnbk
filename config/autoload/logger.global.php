<?php

return [
    'logger' => [
        'app' => [
            'handlers' => []
        ],
        'crawler' => [
            'handlers' => []
        ],
        'subscriptions_checker' => [
            'handlers' => []
        ],
        'notifications' => [
            'handlers' => []
        ]
    ],

    'dependencies' => [
        'factories' => [
            'LoggerFactory' => function() {
                return function($name, $config) {
                    $logger = new Monolog\Logger($name);

                    if (isset($config['handlers'])) {
                        foreach ($config['handlers'] as $handlerConfig) {
                            $handlerName = $handlerConfig['name'];

                            if (isset($handlerConfig['params'])) {
                                $handler = (new \ReflectionClass($handlerName))->newInstanceArgs($handlerConfig['params']);
                            } else {
                                $handler = new $handlerName();
                            }

                            if (isset($handlerConfig['options'])) {
                                foreach ($handlerConfig['options'] as $key => $value) {
                                    $setter = 'set' . ucfirst($key);
                                    if (method_exists($handler, $setter)) {
                                        $handler->$setter($value);
                                    }
                                }
                            }

                            if (isset($handlerConfig['formatter'])) {
                                $formatterName = $handlerConfig['formatter']['name'];
                                $handler->setFormatter(isset($handlerConfig['formatter']['options'])
                                    ? (new \ReflectionClass($formatterName))->newInstanceArgs($handlerConfig['formatter']['options'])
                                    : new $formatterName()
                                );
                            }

                            $logger->pushHandler($handler);
                        }
                    }

                    return $logger;
                };
            },
            'AppLogger' => function($c) {
                $loggerFactory = $c['LoggerFactory'];
                return $loggerFactory('app', $c['config']['logger']['app']);
            },
            'CrawlerLogger' => function($c) {
                $loggerFactory = $c['LoggerFactory'];
                return $loggerFactory('crawler', $c['config']['logger']['crawler']);
            },
            'SubscriptionsCheckerLogger' => function($c) {
                $loggerFactory = $c['LoggerFactory'];
                return $loggerFactory('subscriptions_checker', $c['config']['logger']['subscriptions_checker']);
            },
            'NotificationsLogger' => function($c) {
                $loggerFactory = $c['LoggerFactory'];
                return $loggerFactory('notifications', $c['config']['logger']['notifications']);
            },
        ],
    ],
];
